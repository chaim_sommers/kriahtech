﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

/// <summary>
/// This class contains functions to standardize words for efficient searching
/// and for weeding out words which are syntactically invalid
/// </summary>
public static class WordProcess
{
    public const char SHVA = (char)0X05B0;
    public const char KUMATZ = (char)0X05B8;
    public const char PASACH = (char)0X05B7;
    public const char CHIRIK = (char)0X05B4;
    public const char TZEIREI = (char)0X05B5;
    public const char SEGOL = (char)0X05B6;
    public const char CHOLAM = (char)0X05B9;
    public const char SHURUK = (char)0X05BB;
    public const char CHOLAM_VOV = (char)0XFB4B;
    public const char MELUPUM_VOV = (char)0XFB35;
    public const char CHATAF_KUMATZ = (char)0X05B3;
    public const char CHATAF_PASACH = (char)0X05B2;
    public const char CHATAF_SEGOL = (char)0X05B1;

    public const char DAGESH = (char)0X05BC;
    public const char CHOLAM_VOV_DOT = (char)0X05BA;
    public const char SHIN_DOT = (char)0X05C1;
    public const char SIN_DOT = (char)0X05C2;

    public static readonly char[] Letters = GetLetters();
    public static readonly char[] NekudaLetters = { CHOLAM_VOV, MELUPUM_VOV };
    public static readonly char[] NekudaNonLetters = { KUMATZ, PASACH, SHVA, CHATAF_SEGOL, CHATAF_PASACH, CHATAF_KUMATZ, CHIRIK, TZEIREI, SEGOL, CHOLAM, SHURUK };
    public static readonly char[] Nekudos = NekudaLetters.Concat(NekudaNonLetters).ToArray();
    public const string ValidWordRegExString = @"^{0}?(([{1}][{2}](([{1}][{2}])|[{3}])*{4}{5}?)|([{1}][{6}]))$";
    public static readonly Regex ValidWordRegEx = GetRegExValidWord();

    //The following regexes are used repeatedly, so they are compiled and saved in memory
    private static readonly Regex _regNoBadChars = GetRegExNoBadChars();
    private static readonly Regex _regNekudaDageshFix = GetRegExNekudaDageshFix();
    private static readonly Regex _regLetterCholamShinFix = GetRegLetterCholamShinFix();
    private static readonly Regex _regReverseLetterCholamShin = GetRegReverseLetterCholamShin();
    private static readonly Regex _regCholamSin = GetRegExCholamSin();

    /// <summary>
    /// Standardizes all characters and checks if the word matches regExValidWord.
    /// </summary>
    /// <param name="word"></param>
    /// <returns>the standardized word, or null if the word is invalid</returns>
    public static string CleanWord(string word)
    {
        if (string.IsNullOrWhiteSpace(word))
        {
            return null;
        }
        else
        {
            string stdized = StandardizeCharacters(word);
            return ValidWordRegEx.IsMatch(stdized) ? stdized : null;
        }
    }

    /// <summary>
    /// Calculates the number of letter characters in the word.
    /// Cholam Vovs and Melupum Vovs are also added to the count.
    /// It is assumed that the word has been previously standardized by our StandardizeCharacters function.
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    public static int GetLength(string word)
    {
        return word.Count(c => Letters.Concat(NekudaLetters).Contains(c));
    }

    /// <summary>
    /// To allow copy/paste of search results into programs that don't allow unicode (yup there are still some of those)
    /// this function converts the unicode-only characters to their Windows-1255/Hebrew equivalents.
    /// It also combines two dot characters in a row (such as Sin + Cholam etc.) to a single dot, for neater printing.
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    public static string ConvertWordForDisplay(string word)
    {
        return ReverseLetterCholamShin(word) // Combine a cholam dot next to a shin dot to a single shin dot
            .Replace("שׂ" + CHOLAM, "ש" + SIN_DOT) // Combine the dots of a Sin followed by a Cholam
            .Replace("שׁ", "ש" + SHIN_DOT) // Shin to  Shin + Shin-Dot
            .Replace("שׂ", "ש" + SIN_DOT) // Sin to Shin + Sin-Dot
            .Replace("בּ", "ב" + DAGESH) // Beis to Veis + Dagesh
            .Replace("הּ", "ה" + DAGESH) // Mapik-Hei to Hei + Dagesh
            .Replace("כּ", "כ" + DAGESH) // Kof to Chof + Dagesh
            .Replace("ךּ", "ך" + DAGESH) // End Kof to End Chof + Dagesh
            .Replace("פּ", "פ" + DAGESH) // Pay to Fay + Dagesh
            .Replace("ףּ", "ף" + DAGESH) // End Pay to End Fay + Dagesh
            .Replace("תּ", "ת" + DAGESH) // Tof to Sof + Dagesh
            .Replace("וּ", "ו" + DAGESH) // single melupum to Vov + Dagesh
            .Replace("וֹ", "ו" + CHOLAM_VOV_DOT); // single Cholam-Vov to Vov + Vov-Cholam-Dot
    }

    /// <summary>
    /// Determines if the given word has any nekudos characters.
    /// It is assumed that the word has been previously standardized by our StandardizeCharacters function.
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    public static bool HasAnyNekudos(string word)
    {
        return word.Any(c => Nekudos.Contains(c));
    }

    /// <summary>
    /// Finds any character that is not in our standard for either letters or nekudos.    
    /// </summary>
    /// <returns></returns>
    private static Regex GetRegExNoBadChars()
    {
        string re = string.Format("[^{0}]", new string(Letters.Concat(Nekudos).ToArray()));
        return new Regex(re, RegexOptions.Compiled);
    }

    /// <summary>
    /// This regex captures a nekuda followed by a dagesh, mapik or shin/sin dot.
    /// </summary>
    /// <returns></returns>
    private static Regex GetRegExNekudaDageshFix()
    {
        string re = string.Format("(?<nekuda>[{0}{1}])(?<dagesh>[{2}])",
            new string(NekudaNonLetters),
            (char)0X057C,
            new string(new char[] { DAGESH, SHIN_DOT, SIN_DOT, CHOLAM_VOV_DOT, (char)0X05C4, CHOLAM }));
        return new Regex(re, RegexOptions.Compiled);
    }

    /// <summary>
    /// This regex captures a letter with a cholam followed by a shin. 
    /// For display, the cholam dot should be removed as not to interfere with the shin dot.
    /// </summary>
    /// <returns></returns>
    private static Regex GetRegReverseLetterCholamShin()
    {
        string re = string.Format("(?<letter>[{0}]){1}{2}", new string(Letters), CHOLAM, "שׁ");
        return new Regex(re, RegexOptions.Compiled);
    }

    /// <summary>
    /// This regex captures a Sin in the middle of a word without any nekudah following it.         
    /// We assume that it should have been Sin and Cholam but the dots were combined.
    /// Note, the letter after the Sin is not returned by the regex and is therefore
    /// not overwritten during the replace(look - arounds are fun...)
    /// </summary>
    /// <returns></returns>
    private static Regex GetRegExCholamSin()
    {
        string re = string.Format("{0}(?=[{1}])", "שׂ", new string(Letters));
        return new Regex(re, RegexOptions.Compiled);
    }

    /// <summary>
    /// This regex captures a letter without a nekuda followed by a shin. 
    /// In this case, we assume that a cholam dot should've been placed between the letter and the shin.
    /// </summary>
    /// <returns></returns>
    private static Regex GetRegLetterCholamShinFix()
    {
        string re = string.Format("(?<letter>[{0}]){1}", new string(Letters), "שׁ");
        return new Regex(re, RegexOptions.Compiled);
    }

    /// <summary>
    /// This extension method fixes the issue where a letter without any nikud is followed by a shin. 
    /// In this case we assume that there is supposed to be a cholam after the letter 
    /// but the two dots overlap, so one of them was left out. So we add the cholam dot. 
    /// In The ConvertForDisplay function, we'll take it back out.
    /// An extension function is used to enable chaining all the word replacement functions.
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    private static string FixLetterCholamShin(this string word)
    {
        return _regLetterCholamShinFix.Replace(word, "${letter}" + CHOLAM + "שׁ");
    }

    /// <summary>
    /// This extension method takes a Sin that is not the last letter and is not followed by any nekuda 
    /// and adds a Cholam to it.
    /// It is assumed that there was supposed to be a cholam following the Sin 
    /// but it was removed to prevent the double dot.
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    private static string FixLetterNoNekudaSin(this string word)
    {
        return _regCholamSin.Replace(word, "שׂ" + CHOLAM);
    }

    /// <summary>
    /// Combines a cholam dot next to a shin dot into a single dot.
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    private static string ReverseLetterCholamShin(string word)
    {
        return _regReverseLetterCholamShin.Replace(word, "${letter}ש" + SHIN_DOT);
    }

    /// <summary>
    /// There are many characters available to represent the Hebrew Letters, Dageshes and Nekudos.    
    /// To allow for quick and clean searching, we want to standardize our characters.
    /// For our standard, we use a single character per letter - 
    /// even the dageshed ones (isn't unicode wonderful?)     
    /// So, for all letters that their dageshed versions are pronounced differently (in the Ashkenazi dialect), 
    /// we use the single specialized unicode character to represent the dageshed letter. 
    /// We then remove all stand-alone dagesh, dot and mapik characters.
    /// In addition, many printed texts use shortcuts to prevent overlapping dots and the like.
    /// This may prevent them being considered valid words and conflict with proper searches, 
    /// so we try to identify these and replace them with their proper nikud.
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    private static string StandardizeCharacters(string word)
    {
        string cleaned =
            // First we need to make sure that any dagesh characters are before any nekuda characters - 
            // this is necessary as we will be replacing letter + dagesh with a single unicode character 
            // and the replace won't work if the nekuda is in the middle.
            _regNekudaDageshFix.Replace(word, "${dagesh}${nekuda}").Trim()

            //Replace alternative unicode Hebrew characters with standard ones.
            .Replace((char)0XFB20, 'ע')
            .Replace((char)0XFB21, 'א')
            .Replace((char)0XFB4C, 'ב')
            .Replace((char)0XFB22, 'ד')
            .Replace((char)0XFB23, 'ה')
            .Replace((char)0XFB24, 'כ')
            .Replace((char)0XFB25, 'ל')
            .Replace((char)0XFB26, 'ם')
            .Replace((char)0XFB27, 'ר')
            .Replace((char)0XFB28, 'ת')

            //Replace characters with rafeh with regular ones
            .Replace((char)0XFB4C, 'ב')
            .Replace((char)0XFB4D, 'כ')
            .Replace((char)0XFB4E, 'פ')

            //Replace any dageshed letters where the dagesh is not pronounced 
            //in Ashkenzic pronunciation with non-dageshed letter characters 
            .Replace((char)0XFB30, 'א')
            .Replace((char)0XFB32, 'ג')
            .Replace((char)0XFB33, 'ד')
            .Replace((char)0XFB36, 'ז')
            .Replace((char)0XFB38, 'ט')
            .Replace((char)0XFB39, 'י')
            .Replace((char)0XFB3C, 'ל')
            .Replace((char)0XFB3E, 'מ')
            .Replace((char)0XFB40, 'נ')
            .Replace((char)0XFB41, 'ס')
            .Replace((char)0XFB46, 'צ')
            .Replace((char)0XFB47, 'ק')
            .Replace((char)0XFB48, 'ר')
            .Replace((char)0XFB49, 'שׁ') // "Bald" shin with a dagesh
            .Replace((char)0XFB2C, 'שׁ') // Shin with a dagesh
            .Replace((char)0XFB2D, 'שׂ') // Sin with a dagesh

            //Replace kumatz-alef and pasach-alef single characters with alef plus nekuda.
            .Replace(char.ConvertFromUtf32(0XFB2E), "אַ")
            .Replace(char.ConvertFromUtf32(0XFB2F), "אָ")

            //Replace letter character + dagesh character with a single combined character.
            .Replace("ב" + DAGESH, "בּ") // Veis + Dagesh to Beis
            .Replace("ה" + DAGESH, "הּ") // Hei + Dagesh to Mapik Hei
            .Replace("כ" + DAGESH, "כּ") // Chof + Dagesh to Kof
            .Replace("ך" + DAGESH, "ךּ") // End Chof + Dagesh to End Kof
            .Replace("פ" + DAGESH, "פּ") // Fay + Dagesh to Pay
            .Replace("ף" + DAGESH, "ףּ") // End Fay + Dagesh to End Pay
            .Replace("ת" + DAGESH, "תּ") // Sof + Dagesh to Tof

            //Cholam-vovs and melupums have a few different variations. We replace them all with a single character
            .Replace("ו" + DAGESH, "וּ") // Vov + Dagesh to melupum character
            .Replace("ו" + CHOLAM_VOV_DOT, "וֹ") // Vov + Vov-Cholam-Dot
            .Replace("ו" + (char)0X05C4, "וֹ") // Vov + Upper Dot

            //NOTE: the Vov + Cholam is replaced below - after the shin/sin replacements as they can add a Cholam to a VOV

            //Remove all other dagesh characters
            .Replace(DAGESH.ToString(), "")

            //All Shin and Sins variations are combined to a single character
            .Replace("ש" + SHIN_DOT, "שׁ") // Bald Shin + Shin-Dot
            .Replace("ש" + CHOLAM, "שׂ") // Bald Shin + Cholam Dot to Sin (yes, we've found some of these)
            .Replace("ש" + SIN_DOT, "שׂ") // Bald Shin + Sin-Dot

            //Add a cholam dot between a letter without a nekuda followed by a shin
            .FixLetterCholamShin()

            //Add a cholam to a Sin that is not the last letter and is not followed by a nekuda
            .FixLetterNoNekudaSin()

            //Replace any "bald" Shins that are left, with a Shin character.
            .Replace('ש', 'שׁ')

            //Replace Vov + Upper Cholam with single character. 
            //This is done down here as FixLetterCholamShin() may have added a Cholam to the vov. 
            .Replace("ו" + CHOLAM, "וֹ")

            //Special characters
            .Replace((char)0X057C, KUMATZ) // Kamatz Kattan to regular Kamatz
            .Replace(char.ConvertFromUtf32(0XFB4F), "אל"); // ﭏ to alef + lamed

        //Remove any characters that are not either a letter or a nekuda (like trup etc.) 
        //and return the clean word.
        return _regNoBadChars.Replace(cleaned, "");
    }

    private static char[] GetLetters()
    {
        SortedSet<char> l = new SortedSet<char>();

        //add letters א through ש (not including ש - it is added below with the dageshed letters)
        for (int i = (int)'א'; i < (int)'ש'; i++)
        {
            l.Add((char)i);
        }
        l.Add('ת');

        //Add Dageshed letters such as Beis, Mapik Hei, Kof, Pay, Shin, Sin etc.
        return l.Concat(new char[] { 'בּ', 'הּ', 'ךּ', 'כּ', 'פּ', 'ףּ', 'שׁ', 'שׂ', 'תּ' }).ToArray();
    }

    /// <summary>
    /// This regular expression attempts to determine if the given word is syntactically valid.
    /// It is assumed that the word has been previously standardized by our StandardizeCharacters function.
    /// </summary>
    /// <returns></returns>
    private static Regex GetRegExValidWord()
    {
        // {0} The vov-dagesh is the only nekuda that is valid by itself; but only as the first letter of a word.
        char melupum = MELUPUM_VOV;

        // {1} The letters הּ,ם,ן,צ,פ,ך, are only valid as the last letter of a word
        string validMiddleLetters = "אבגדהוזחטיכלםמנסעפצקרתבּוּכּפּשׁשׂתּוֹ",

        // {2} All the nekudos are valid in middle of a word.
        validMiddleNekudos = new string(Nekudos),

        // {3} The letters that are valid in the middle of a word without a nekuda following them (without even a shva nach)
        validStandaloneLetters = "יאוֹר",

        // {4} For the last letter of a word there are 3 options: 
        //      1) any letter that does not have an end letter. 
        //      2) An end letter that is not followed by a וּ or וֹ 
        //      3) מ,נ,צ,פ,כ that is followed by וּ or וֹ
        validEndLetters = string.Format("([{0}]|([{1}](?![{2}]))|([{3}](?=[{2}])))",
            "אבגדהוזחטילסעקרתבּהּשׁשׂתּ", "ךךּםןףץ", "וּוֹ", "מנצפפּככּ"),

        // {5} Optional nekuda that is the last character of a word.
        //     May be Kumatz, Pasach, Shva, Vov-Cholam and Vov-Dagesh:
        //      * Shva can follow any letter. 
        //      * The dotted vovs are allowed after any letter besides ךךּםןףץ. (The "validEndLetters" section takes care of that caveat.)
        //      * Kumatz and Pasach are only allowed after the letters חההּחעןךּךתתּ. A positive look-behind is used to enforce this.
        validEndNekudosRegEx = string.Format("([{0}{1}]|(?<=[{2}])[{3}{4}])",
            SHVA, "וּוֹ", "חההּחעןךּךתתּ", KUMATZ, PASACH),

        // {6} The final character of a two letter word where the first letter has no nekudos
        //     can only be a vov-cholam or vov-melupum
        validLastNekudosTwoLetterWord = "וּוֹ";

        return new Regex(String.Format(ValidWordRegExString,
            melupum,
            validMiddleLetters,
            validMiddleNekudos,
            validStandaloneLetters,
            validEndLetters,
            validEndNekudosRegEx,
            validLastNekudosTwoLetterWord), RegexOptions.Compiled);
    }
}

