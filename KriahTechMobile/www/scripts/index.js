﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";
    
        document.addEventListener('deviceready', onDeviceReady.bind(this), false);
    
        function onDeviceReady() {
            // Handle the Cordova pause and resume events
            document.addEventListener('pause', onPause.bind(this), false);
            document.addEventListener('resume', onResume.bind(this), false);
        };
    
        function onPause() {
            // TODO: This application has been suspended. Save application state here.
        };
    
        function onResume() {
            // TODO: This application has been reactivated. Restore application state here.
        };
    


    document.addEventListener('init', function (event) {
        console.log(event.target.id + ' init!');
        switch (event.target.id) {
            case 'pgChooseLetters':
                fillLetters();
                setState();
                $('#btnSelectAll').on('click', function () {
                    $('#tblLetters td').filter(function () {
                        return $(this).data('state') === 0;
                    }).data('state', 1).css('backgroundColor', '#ee0');
                });

                $('#btnUnselectAll').on('click', function () {
                    $('#tblLetters td').data('state', 0).css('backgroundColor', '');
                });
                break;
            case 'pgViewWords':
                filterWords();
                break;
        }
    });

    function fillLetters() {
        var html = '<tr>',
            letters = 'אבּבגדההּוזחטיכּךּכךלמםנןסעפּפףצץקרשׁשׂתּת',
            nekudos = String.fromCharCode(0X05B0, 0X05B1, 0X05B2, 0X05B3, 0X05B4, 0X05B5, 0X05B6, 0X05B7, 0X05B8, 0X05B9),
            letterNekudos = ['וּ..', '..וּ.', 'וֹ'],
            endings = '',
            letter = '',
            counter = 0;
        for (var i = 0; i < letters.length; i++) {
            letter = letters[i];
            if (counter && (counter % 5 == 0)) {
                html += '</tr><tr>';
            }
            html += '<td data-value="' + letter +
                '"><div class="nekLeters">' + letter + '</div></td>';
            counter++;
        }
        for (var i = 0; i < nekudos.length; i++) {
            letter = nekudos[i];
            if (counter && (counter % 5 == 0)) {
                html += '</tr><tr>';
            }
            html += '<td data-value="' + letter +
                '"><i class="fa fa-dot-circle-o"></i><div class="nek">' + letter + '</div></td>';
            counter++;
        }
        for (var i = 0; i < letterNekudos.length; i++) {
            letter = letterNekudos[i];
            if (counter && (counter % 5 == 0)) {
                html += '</tr><tr>';
            }
            html += '<td data-value="' + letter +
                '"><div class="nekLeters">' + letter + '</div></td>';
            counter++;
        }

        html += '</tr>';
        $('#tblLetters').html(html);
    }

    function setState() {
        $('#tblLetters td').data('state', 0).on('click', function () {
            var i = $(this);
            switch (i.data('state')) {
                case 0:
                    i.data('state', 1);
                    i.css('backgroundColor', '#ee0');
                    break;
                case 1:
                    i.data('state', 2);
                    i.css('backgroundColor', '#5a5');
                    break;
                case 2:
                    i.data('state', 0);
                    i.css('backgroundColor', '');
                    break;
            }
        });
    }

    function viewWords() {
        var selected = $('#tblLetters td').filter(function () {
            return $(this).data('state') !== 0;
        });
        if (selected.length === 0) {
            selected = $('#tblLetters td');
        }
        selected = selected.map(function () {
            return $(this).data(value);
        });
    }
})();