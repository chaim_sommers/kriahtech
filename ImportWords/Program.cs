﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace GetUniqueWordsinWordDocument
{
    class Program
    {
        private static readonly Regex regAllPuntuaction = new Regex(@"[^\w\s]", RegexOptions.Compiled);
        private static char _cursorBar;
        //.doc, .pub and a few file types start with these bytes
        private static readonly byte[] doc = { 0XD0, 0XCF, 0X11, 0XE0, 0XA1, 0XB1, 0X1A, 0XE1 };
        //.xls and .ppt files start with 512 0 bytes
        private static readonly byte[] xls = { 0, 0, 0, 0, 0, 0, 0, 0 };
        //Most compressed formats (.docx, .pptx, .xlsx, .zip, .jar, .epub etc.) start with these bytes.
        private static readonly byte[] docx = { 0X50, 0X4B, 0X03, 0X04 };
        private static bool _noWordDoc;
        private static bool _noErrDoc;
        private static bool _ignoreNoNikud;

        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: ImportWords.exe SourcePath DestinationPath");
                return;
            }

            _noWordDoc = args.Contains("-noWordDoc");
            _noErrDoc = args.Contains("-noErrDoc");
            _ignoreNoNikud = args.Contains("-ignoreNoNikud");

            string source = args[0], dest = args[1];
            if (!Path.IsPathRooted(source))
            {
                source = Path.Combine(Directory.GetCurrentDirectory(), Path.GetDirectoryName(source), Path.GetFileName(source));
            }
            if (!Path.IsPathRooted(dest))
            {
                dest = Path.Combine(Directory.GetCurrentDirectory(), Path.GetDirectoryName(dest), Path.GetFileName(dest));
            }
            if ((!File.Exists(source)) && Directory.Exists(source))
            {
                if ((!File.Exists(dest)) && !Directory.Exists(dest))
                {
                    Directory.CreateDirectory(dest);
                    Console.WriteLine("\nCreated folder: {0}", dest);
                }
                else if ((File.GetAttributes(dest) & FileAttributes.Directory) != FileAttributes.Directory)
                {
                    Console.WriteLine("If the first argument is a Folder, both arguments need to be Folders.");
                    return;
                }

                foreach (var file in Directory.GetFiles(source))
                {
                    try
                    {
                        DoImport(file, Path.Combine(dest, Path.GetFileNameWithoutExtension(file)) + ".txt");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("\n\nFailed to import {0}. Message: {1}", file, ex.Message);
                    }
                }
            }
            else if (File.Exists(source))
            {
                DoImport(source, dest);
            }
            else
            {
                Console.WriteLine("\nWe can not find {0}.", source);
            }


            Console.WriteLine("\nPress <ENTER> to exit.......");
            Console.ReadLine();
        }

        private static void DoImport(string source, string dest)
        {
            if (File.Exists(dest))
            {
                Console.WriteLine("\n\nThe file {0} already exists.\nPress 'Y' to continue or any other key to exit. ", dest);

                ConsoleKeyInfo answer = Console.ReadKey();
                if (answer.Key != ConsoleKey.Y)
                {
                    return;
                }
            }
            if (!File.Exists(source))
            {
                Console.WriteLine("We could not find the file {0}. Please check the file path.", source);
                return;
            }
            Console.WriteLine("\n\n{0}\n\nStarting import from {1} into {2}\nDepending on the size of the file, this may take a minute or two....\n{0}\n\n",
                new string('*', 50), Path.GetFileName(source), dest);

            List<string>[] wordLists = GetWordsAsync(source, dest).Result;

            if (!_noWordDoc && wordLists[0].Count > 0)
            {
                File.WriteAllText(dest,
                    string.Join(Environment.NewLine, (from w in wordLists[0] orderby WordProcess.GetLength(w), w select w)));

                Console.WriteLine("\n\n{0} words have been successfully exported to {1}", wordLists[0].Count, dest);
            }
            else if (wordLists[0].Count == 0)
            {
                Console.WriteLine("THERE WERE NO VALID WORDS TO IMPORT IN THE DOCUMENT");
            }
            if (!_noErrDoc && wordLists[1].Count > 0)
            {
                string errorFilePath = Path.Combine(Path.GetDirectoryName(dest), Path.GetFileNameWithoutExtension(source) + "_ERROR_WORDS.txt");
                File.WriteAllText(errorFilePath, string.Join(Environment.NewLine, wordLists[1]));

                Console.WriteLine("\n\nThere were {0} invalid words in the file.\nA document containing those words has also been created.", wordLists[1].Count);
                if (_ignoreNoNikud)
                {
                    Console.WriteLine("NOTE: The invalid word document does not contain words that do not have any nekudos.");
                }
            }
            else if (wordLists[1].Count() == 0)
            {
                Console.WriteLine("There were no invalid words in the document.");
            }
        }

        private static async System.Threading.Tasks.Task<List<string>[]> GetWordsAsync(string docPath, string savePath)
        {
            List<string> wordsList = new List<string>();
            List<string> errWordsList = new List<string>();
            string text = IsWordFile(docPath) ? ExtractTextOfWordDocument(docPath) : File.ReadAllText(docPath);
            int validWordCounter = 0,
                invalidWordCounter = 0;
            if (!string.IsNullOrEmpty(text))
            {
                Console.WriteLine("Successfully extracted text from {0}.\nStarting word import...", Path.GetFileName(docPath));
                int lineCounter = 0;
                //To be able to accurately report bad word line numbers, we split the text by the current systems new-line character/s
                foreach (string line in text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
                {
                    lineCounter++;
                    if (!string.IsNullOrWhiteSpace(line.Trim()))
                    {
                        //Replace all punctuation, single carriage-return and single line-feed characters with a space.
                        string totaltext = regAllPuntuaction.Replace(line, " ").Replace("\r", " ").Replace("\n", " ");
                        if (!string.IsNullOrWhiteSpace(totaltext))
                        {
                            int wordCounter = 0;
                            //Get the individual words
                            foreach (string w in totaltext.Trim().Split(' '))
                            {
                                wordCounter++;
                                //All double spaces will be ignored
                                if (!string.IsNullOrWhiteSpace(w))
                                {
                                    try
                                    {
                                        //Standardize the word and then determine if it is syntactically valid
                                        string wordCleaned = WordProcess.CleanWord(w.Trim());
                                        //WordProcess.CleanWord returns null for invalid words 
                                        if (wordCleaned == null)
                                        {
                                            //The invalid word is added to the error-word list.
                                            //If the word does not have any nekudos, it will only be reported if _ignoreNoNikud is false.                                            
                                            if (WordProcess.HasAnyNekudos(w) || !_ignoreNoNikud)
                                            {
                                                errWordsList.Add(string.Format("Line {0} Word {1}: |-- {2} --| Characters: {3}",
                                                    lineCounter, wordCounter, w, SplitIntoCharacters(w)));
                                                invalidWordCounter++;
                                            }
                                        }
                                        else if (!wordsList.Contains(wordCleaned))
                                        {
                                            wordsList.Add(wordCleaned);
                                            validWordCounter++;
                                        }
                                    }
                                    catch
                                    {
                                        errWordsList.Add(string.Format("!ERROR! - Line {0} Word {1}: |-- {2} --| Characters: {3}",
                                                   lineCounter, wordCounter, w, SplitIntoCharacters(w)));
                                        invalidWordCounter++;
                                    }
                                    await System.Threading.Tasks.Task.Run(new Action(() =>
                                    {
                                        Console.SetCursorPosition(0, Console.CursorTop);
                                        SetCursorBar(validWordCounter);
                                        Console.Write("{0} Total distinct valid words: {1}. Total invalid words found: {2}",
                                                _cursorBar, validWordCounter, invalidWordCounter);
                                    }));
                                }
                            }
                        }
                    }
                }
            }

            return new List<string>[] { wordsList, errWordsList };
        }

        private static bool IsWordFile(string docPath)
        {
            string ext = Path.GetExtension(docPath.ToLower());
            if (ext == ".rtf")
            {
                return true;
            }
            byte[] buf = new byte[8];
            using (var sr = File.OpenRead(docPath))
            {
                sr.Read(buf, 0, buf.Length);
                sr.Close();
            }
            if (buf.SequenceEqual(doc) || buf.SequenceEqual(xls) || buf.Take(docx.Length).SequenceEqual(docx))
            {
                return true;
            }

            return false;
        }

        private static string ExtractTextOfWordDocument(string docPath)
        {
            string text = null;
            Application app = null;
            Document doc = null;
            object miss = Missing.Value,
                   path = docPath,
                   readOnly = true;
            try
            {
                app = new Application();
                doc = app.Documents.Open(ref path, ref miss, ref readOnly,
                ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);
                text = doc.Content.Text;
                object save = false;
                doc.Close(ref save);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.FinalReleaseComObject(doc);
                doc = null;

                app.Quit();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.FinalReleaseComObject(app);
                app = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nAn exception occurred. Information: {0}", ex.Message);
                if (doc != null)
                {
                    try
                    {
                        doc.Close();
                    }
                    catch { }
                }
                if (app != null)
                {
                    try
                    {
                        app.Quit();
                    }
                    catch { }
                }
            }

            return text;
        }

        private static void SetCursorBar(int i)
        {
            switch (i % 400)
            {
                case 0:
                    _cursorBar = '/';
                    break;
                case 100:
                    _cursorBar = '-';
                    break;
                case 200:
                    _cursorBar = '\\';
                    break;
                case 300:
                    _cursorBar = '|';
                    break;
            }
        }

        /// <summary>
        /// For displaying the characters in a hebrew word.
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private static string SplitIntoCharacters(string word)
        {
            string s = "";
            for (int i = word.Length - 1; i >= 0; i--)
            {
                char c = word[i];
                if (s.Length > 0)
                {
                    s += " | ";
                }
                if (WordProcess.NekudaNonLetters.Contains(c))
                {
                    s += " " + c;
                }
                else
                {
                    s += c;
                }
            }
            return s;
        }
    }
}


