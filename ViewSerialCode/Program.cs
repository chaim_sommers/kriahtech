﻿using System;

namespace GetMachineSerialCode
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;
            while (!exit)
            {
                GetSerialCodeFromInitCode();
                Console.WriteLine("\nPress 'Y' to get another code, or any other key to exit...");
                exit = Console.ReadKey().Key != ConsoleKey.Y;
            }
        }

        private static void GetSerialCodeFromInitCode()
        {
            Console.WriteLine("\nInitiation Key: ");
            var init = Console.ReadLine();
            try
            {
                string[] initKeyParts = init.Split('Y');
                string serialCode = Oshek.OshekManager.GetSerialCode(initKeyParts[0], initKeyParts[1]);
                Console.WriteLine("Serial Code: {0}", serialCode);
            }
            catch
            {
                Console.WriteLine("\nIncorrect Initiation Key");
            }
        }
    }
}
