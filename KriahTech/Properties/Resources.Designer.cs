﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KriahTech.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("KriahTech.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to אַב
        ///אָב
        ///אוֹ
        ///אוּד
        ///אוֹר
        ///אוּר
        ///אוֹת
        ///אָז
        ///אָח
        ///אִי
        ///אֵי
        ///אֶל
        ///אַל
        ///אִם
        ///אֵם
        ///אֹם
        ///אַף
        ///אָף
        ///אֵשׁ
        ///אֵת
        ///אֶת
        ///בָא
        ///בָּא
        ///בֹּא
        ///בּאוּ
        ///בַד
        ///בַּד
        ///בָּד
        ///בָהּ
        ///בֵּהּ
        ///בַּה
        ///בַּהּ
        ///בָּה
        ///בָּהּ
        ///בוֹ
        ///בּוֹ
        ///בוּז
        ///בּוּל
        ///בּוּץ
        ///בוֹר
        ///בּוּר
        ///בִי
        ///בִּי
        ///בֵּי
        ///בַל
        ///בַּל
        ///בָם
        ///בָּם
        ///בֵן
        ///בֶן
        ///בִּן
        ///בֵּן
        ///בֶּן
        ///בַּר
        ///בָּר
        ///בֹּר
        ///בַת
        ///בַּת
        ///גַב
        ///גַג
        ///גָג
        ///גָד
        ///גוֹי
        ///גוּף
        ///גוּשׁ
        ///גֵז
        ///גָז
        ///גֶט
        ///גַל
        ///גַם
        ///גַן
        ///גַס
        ///גָס
        ///גֶץ
        ///גֵר
        ///גָר
        ///גַת
        ///דָא
        ///דָג
        ///דַד
        ///דוּד
        ///דוּן
        ///דוֹר
        ///דוּר
        ///דוּת
        ///דִי
        ///דֵי
        ///דַי
        ///דַל
        ///דָל
        ///דַם
        ///דָם
        ///דֹם
        ///דָן
        ///דַע
        ///דַף
        ///דַק
        ///דָק
        ///דָר
        ///דֹר
        ///דַשׁ
        ///דָשׁ
        ///דַ [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string General_List {
            get {
                return ResourceManager.GetString("General_List", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html&gt;
        ///&lt;html&gt;
        ///&lt;head&gt;
        ///    &lt;title&gt;Kriah Tech&lt;/title&gt;
        ///    &lt;meta charset=&quot;utf-8&quot; /&gt;
        ///    &lt;meta http-equiv=&quot;X-UA-Compatible&quot; content=&quot;IE=Edge&quot; /&gt;
        ///    &lt;script type=&quot;text/javascript&quot;&gt;
        ///        function hideNavBar() {
        ///            document.getElementById(&apos;navBar&apos;).style.display = &apos;none&apos;;
        ///        }
        ///        function showStartup() {
        ///            hideNavBar();
        ///            document.getElementById(&apos;divStartup&apos;).style.display = &apos;block&apos;;
        ///        }
        ///    &lt;/script&gt;
        ///    &lt;style type=&quot;text/css&quot;&gt;
        ///        @medi [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string HTML_TEMPLATE {
            get {
                return ResourceManager.GetString("HTML_TEMPLATE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap KriahTechLogo {
            get {
                object obj = ResourceManager.GetObject("KriahTechLogo", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap SearchHide {
            get {
                object obj = ResourceManager.GetObject("SearchHide", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap SearchShow {
            get {
                object obj = ResourceManager.GetObject("SearchShow", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap settings {
            get {
                object obj = ResourceManager.GetObject("settings", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap SettingsLarger {
            get {
                object obj = ResourceManager.GetObject("SettingsLarger", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
