﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace KriahTech
{
    public static class WordsFunctions
    {
        /// <summary>
        /// Find the words in the current file that match the given search criteria.
        /// Sets Program.CurrentResults to a list of words grouped by word length.
        /// </summary>
        /// <param name="regexp"></param>
        /// <param name="mustHave"></param>
        public static void FindInCurrentFile(string regexp, string mustHave = null)
        {
            string path = Path.Combine(Program.FilesFolder, Properties.Settings.Default.CurrentFileName);
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Could not find " + path);
            }

            List<string> foundList = new List<string>();
            Regex re = new Regex(regexp, RegexOptions.Compiled);

            using (var sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    string word = sr.ReadLine();
                    if (word == null) //End of file
                    {
                        break;
                    }
                    else if ((!string.IsNullOrWhiteSpace(word)) &&
                            re.IsMatch(word) &&
                            (string.IsNullOrEmpty(mustHave) || word.Intersect(mustHave).Count() == mustHave.Length))
                    {
                        foundList.Add(word);
                    }
                }
                sr.Close();
            }

            Program.CurrentResults = foundList.GroupBy(w => WordProcess.GetLength(w));
        }

        /// <summary>
        /// Remove any words in the CurrentList that do not match the given regular expression
        /// </summary>
        /// <param name="regexp"></param>
        public static void FilterCurrentList(string regexp)
        {
            Regex re = new Regex(regexp, RegexOptions.Compiled);
            Program.CurrentResults = Program.CurrentResults.SelectMany(g => g)
                .Where(w => re.IsMatch(w))
                .GroupBy(w => WordProcess.GetLength(w));
        }

        /// <summary>
        /// If the search results are to be displayed any differently than they are listed,
        /// this function does that conversion.
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string ConvertWordForDisplay(string word)
        {
            if (Properties.Settings.Default.HideChatafs)
            {
                word = word.Replace((char)0x05b1, (char)0x05b6) //Segol
                    .Replace((char)0x05b2, (char)0x05b7) //Pasach
                    .Replace((char)0x05b3, (char)0x05b8); //Kumatz
            }
            // Converts unicode-only characters to their Windows-1255/Hebrew equivalents.
            return WordProcess.ConvertWordForDisplay(word);
        }
    }
}
