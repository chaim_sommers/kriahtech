﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;

namespace KriahTech
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class ClientController
    {
        public event Action<string> CurrentHtmlChanged;

        public int CurrentPageNumber { get; set; }

        public string CurrentHtml
        {
            get
            {
                return this._currentHtml;
            }
            set
            {
                this._currentHtml = value; if (this.CurrentHtmlChanged != null)
                {
                    this.CurrentHtmlChanged(this._currentHtml);
                }
            }
        }

        private string _currentHtml;
        private readonly string[] _colors = { "Red", "Blue", "DarkGreen", "Maroon", "Orange", "Purple" };

        public void CopyAll()
        {
            Clipboard.SetText(String.Join(" ", Program.CurrentResults.SelectMany(wg => wg
                .Select(w => WordsFunctions.ConvertWordForDisplay(w)).ToArray())));
        }

        public void IgnoreWord(string word)
        {
            if ((!String.IsNullOrWhiteSpace(word)) &&
                !Properties.Settings.Default.IgnoredWords.Contains(word))
            {
                Properties.Settings.Default.IgnoredWords.Add(word);
            }
        }

        public void ShowIgnored()
        {
            var origLen = Properties.Settings.Default.IgnoredWords.Count;
            var dr = (new frmIgnoredWords()).ShowDialog();
            if (Properties.Settings.Default.IgnoredWords.Count != origLen)
            {
                RefreshHtml();
            }
        }

        public void CopyPage()
        {
            Clipboard.SetText(String.Join(" ", Program.CurrentResults.SelectMany(wg => wg
                    .Select(w => WordsFunctions.ConvertWordForDisplay(w))
                    .Skip(this.CurrentPageNumber * Properties.Settings.Default.NumberOfWordsPerGroup)
                    .Take(Properties.Settings.Default.NumberOfWordsPerGroup))));
        }

        public void Next()
        {
            this.CurrentPageNumber++;
            this.RefreshHtml();
        }

        public void Previous()
        {
            if (this.CurrentPageNumber > 0)
            {
                this.CurrentPageNumber--;
                this.RefreshHtml();
            }
        }

        public void ChangeFont(int size)
        {
            Properties.Settings.Default.FontSize = size;
        }

        public void ChangeColumns(int size)
        {
            Properties.Settings.Default.NumberOfColumns = size;
        }

        public void ChangeGroupSize(int size)
        {
            Properties.Settings.Default.NumberOfWordsPerGroup = size;
        }

        public void RefreshHtml()
        {
            this.CurrentHtml = this.GetHtml();
        }

        public void ShowSettings()
        {
            Program.ShowSettings();
        }

        private string GetHtml()
        {
            StringBuilder html = new StringBuilder();
            int counter, total = 0;

            foreach (var wordGroup in Program.CurrentResults)
            {
                //Note: The filter for the ignored words list needs to be here at the display level,
                //because we acquire the words to ignore from the browser client - which has the words after
                //they have been converted using "WordsFunctions.ConvertWordForDisplay".
                IEnumerable<string> wordsList = wordGroup
                    .Where(w => !Properties.Settings.Default.IgnoredWords.Contains(
                        WordsFunctions.ConvertWordForDisplay(w)));
                IEnumerable<string> pagedWords = wordsList
                    .Skip(this.CurrentPageNumber * Properties.Settings.Default.NumberOfWordsPerGroup)
                    .Take(Properties.Settings.Default.NumberOfWordsPerGroup);
                int wordCount = wordsList.Count();

                if (html.Length > 0)
                {
                    html.Append("</tr>");
                }

                html.AppendFormat(@"<tr><td class='tdSeperator' colspan='{0}'>", Properties.Settings.Default.NumberOfColumns);
                if (pagedWords.Count() == 0)
                {
                    html.AppendFormat("<div class='divGroupHeader'>No more {0}  letter words</div>",
                        wordGroup.Key);
                }
                else
                {
                    int startNumber = (this.CurrentPageNumber * Properties.Settings.Default.NumberOfWordsPerGroup),
                        endNumber = Math.Min(wordCount, (startNumber + Properties.Settings.Default.NumberOfWordsPerGroup));
                    html.AppendFormat("<div class='divGroupHeader'>{0} to {1} out of {2} words with {3} letters</div>",
                        startNumber + 1, endNumber, wordCount, wordGroup.Key);
                }
                html.Append("</td></tr>");

                if (pagedWords.Count() > 0)
                {
                    html.Append("<tr>");

                    counter = 0;
                    foreach (string word in pagedWords.Select(w => WordsFunctions.ConvertWordForDisplay(w)))
                    {
                        if (counter > 0 && counter % Properties.Settings.Default.NumberOfColumns == 0)
                        {
                            html.Append("</tr><tr>");
                        }
                        counter++;
                        total++;
                        html.Append("<td class=\"tdWord\"");
                        if (Properties.Settings.Default.ShowResultsInColor)
                        {
                            html.AppendFormat(" style=\"color:{0};\"",
                                this._colors[total % this._colors.Length]);
                        }
                        html.AppendFormat(@"><div>
                            <a onclick=""javascript:ignore('{0}', this);return false;"">
                                Ignore this word</a>{0}</div></td>", word);
                    }
                }
            }

            return html.Length > 0 ? Properties.Resources.HTML_TEMPLATE
                .Replace("__FONT_SIZE__", Properties.Settings.Default.FontSize.ToString())
                .Replace("__FONT_FAMILY__", "'" + Properties.Settings.Default.FontFamily + "'" +
                    (Properties.Settings.Default.FontFamily != "Times New Roman" ? ",'Times New Roman'" : ""))
                .Replace("__NUMBER_OF_COLUMNS__", Properties.Settings.Default.NumberOfColumns.ToString())
                .Replace("__GROUP_SIZE__ ", Properties.Settings.Default.NumberOfWordsPerGroup.ToString())
                .Replace("__SETTINGS_IMG_PATH__", this.GetSettingsImagePath())
                .Replace("__TABLE_BODY__", html.ToString()) : null;
        }

        private string GetSettingsImagePath()
        {
            return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                @"Resources\Settings.png");
        }
    }
}
