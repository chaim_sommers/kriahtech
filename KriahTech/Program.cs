﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KriahTech
{
    static class Program
    {
        public static readonly string FilesFolder = Path.Combine(
            Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            Properties.Settings.Default.WordFilePath);
        public static IEnumerable<IGrouping<int, string>> CurrentResults { get; set; }
        public static bool IsAdminRun { get; private set; }


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (Properties.Settings.Default.NeedsUpdate)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.Save();
            }

            if (args.Contains("-admin"))
            {
                IsAdminRun = true;
            }

            CheckDefaultFile();
            CheckRandomKey();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (true || HasCorrectSerialCode())
            {
                Application.Run(new Form1());
            }
            else
            {
                Application.Run(new frmEnterSerialCode());
            }
        }

        private static void CheckDefaultFile()
        {
            string generalListPath = Path.Combine(FilesFolder, "General List.txt");
            if (!File.Exists(generalListPath))
            {
                File.WriteAllText(generalListPath, Properties.Resources.General_List);
            }
            if (string.IsNullOrWhiteSpace(Properties.Settings.Default.CurrentFileName) ||
                !File.Exists(Path.Combine(FilesFolder, Properties.Settings.Default.CurrentFileName)))
            {
                Properties.Settings.Default.CurrentFileName = Path.GetFileName(generalListPath);
            }
        }

        private static void CheckRandomKey()
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.OshekHiR))
            {
                string random = Guid.NewGuid().ToString().Replace("-", "").Substring(6, 11).ToUpper();
                Properties.Settings.Default.OshekHiR = Oshek.OshekManager.GetMuxedForStorage(random);
            }
        }
        
        public static bool HasCorrectSerialCode()
        {
            if (string.IsNullOrWhiteSpace(Properties.Settings.Default.OshekLoS) ||
                string.IsNullOrWhiteSpace(Properties.Settings.Default.OshekHiR))
            {
                return false;
            }

            var origCode = Oshek.OshekManager.UnMuxFromStorage(Properties.Settings.Default.OshekLoS);
            string origRandom = Oshek.OshekManager.UnMuxFromStorage(Properties.Settings.Default.OshekHiR);

            if (Oshek.OshekManager.IsValidSerialCode(origCode, origRandom))
            {
                return true;
            }
            return false;
        }

        public static void ShowSettings()
        {
            new frmSettings().Show();
        }
    }
}
