﻿using System;
using System.Windows.Forms;

namespace KriahTech
{
    public partial class frmViewSerialCode : Form
    {
        public frmViewSerialCode()
        {
            InitializeComponent();
        }

        private void frmViewSerialCode_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string[] initKeyParts = this.tbInitKey.Text.Split('Y');
                this.lblSerialCode.Text =
                    Oshek.OshekManager.GetSerialCode(initKeyParts[0], initKeyParts[1]);
            }
            catch
            {
                this.lblError.Visible = true;
            }
        }        

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Clipboard.SetText(this.lblSerialCode.Text);
        }

        private void tbInitKey_TextChanged(object sender, EventArgs e)
        {
            this.lblError.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
