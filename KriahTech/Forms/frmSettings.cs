﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KriahTech
{
    public partial class frmSettings : Form
    {
        private class SingleFile
        {
            public string FilePath { get; set; }
            public string FileName
            {
                get
                {
                    return Path.GetFileNameWithoutExtension(this.FilePath);
                }
            }
        }

        private List<SingleFile> _filesList;

        public frmSettings()
        {
            InitializeComponent();
            this.numericUpDown1.DataBindings.Add(new Binding("Value", Properties.Settings.Default, "NumberOfWordsPerGroup", true, DataSourceUpdateMode.OnPropertyChanged));
            this.numericUpDown1.Value = Properties.Settings.Default.NumberOfWordsPerGroup;
            this.numericUpDown3.DataBindings.Add(new Binding("Value", Properties.Settings.Default, "FontSize", true, DataSourceUpdateMode.OnPropertyChanged));
            this.numericUpDown3.Value = Properties.Settings.Default.FontSize;
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            var files = Directory.GetFiles(Program.FilesFolder);
            this._filesList = files.Select(f => new SingleFile { FilePath = f }).ToList();
            this.cmbFiles.DataSource = this._filesList;
            this.cmbFiles.DisplayMember = "FileName";
            this.cmbFiles.ValueMember = "FilePath";
            this.cmbFiles.SelectedItem = this._filesList.SingleOrDefault(f =>
                Path.GetFileName(f.FilePath) == Properties.Settings.Default.CurrentFileName);
            cmbFiles.SelectedIndexChanged += cmbFiles_SelectedIndexChanged;

            var installedFontCollection = new InstalledFontCollection().Families;
            this.cmbFontName.DataSource = installedFontCollection;
            this.cmbFontName.DisplayMember = "Name";
            this.cmbFontName.ValueMember = "Name";
            this.cmbFontName.SelectedItem = installedFontCollection.SingleOrDefault(f =>
                f.Name == Properties.Settings.Default.FontFamily);
            this.cmbFontName.SelectedIndexChanged += cmbFontName_SelectedIndexChanged;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reload();
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reset();
        }

        private void cmbFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            var path = ((SingleFile)this.cmbFiles.SelectedItem).FilePath;
            Properties.Settings.Default.CurrentFileName = Path.GetFileName(path);
        }
        private void cmbFontName_SelectedIndexChanged(object sender, EventArgs e)
        {
            var fontFamilyName = ((FontFamily)this.cmbFontName.SelectedItem).Name;
            Properties.Settings.Default.FontFamily = fontFamilyName;
        }

        private void llOpenIgnoreList_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var origLen = Properties.Settings.Default.IgnoredWords.Count;
            var dr = (new frmIgnoredWords()).ShowDialog(this);
            if(Properties.Settings.Default.IgnoredWords.Count != origLen)
            {
                Form1.ClientController.RefreshHtml(); 
            }
        }
    }
}
