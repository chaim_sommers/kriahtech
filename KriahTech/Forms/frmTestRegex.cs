﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace KriahTech
{
    public partial class frmTestRegex : Form
    {
        private string _regExstring = WordProcess.ValidWordRegExString;

        public frmTestRegex()
        {
            InitializeComponent();
            this.txtPHValues.Text = this.GetPlaceHolderValues();
            this.textBox2.Text = this._regExstring;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.lbInfo.Items.Clear();
            this.lbInfo.Items.Add("Length: " + this.textBox1.Text.Trim().Length.ToString());
            int counter = 1;
            foreach (char c in this.textBox1.Text.Trim())
            {
                var info = "";
                info += counter.ToString() + ":     0X";
                info += ((int)c).ToString("X") + "     ";
                if (WordProcess.Nekudos.Contains(c))
                {
                    info += " ";
                }
                info += c;
                counter++;
                this.lbInfo.Items.Add(info);
            }

            if (WordProcess.ValidWordRegEx.IsMatch(this.textBox1.Text.Trim()))
            {
                this.label1.Text = "Yes";
                this.label1.ForeColor = Color.DarkGreen;
            }
            else
            {
                this.label1.Text = "Nope";
                this.label1.ForeColor = Color.DarkRed;
            }
            this.ShowResults();
        }        

        private string GetPlaceHolderValues()
        {
            /*The letters הּ,ם,ן,צ,פ,ך, are only valid as the last letter of a word*/
            string validMiddleLetters = "אבגדהוזחטיכלםמנסעפצקרתבּוּכּפּשׁשׂתּוֹ",
            /*All the nekudos are valid in middle of a word.*/
            validMiddleNekudos = string.Join(" ", WordProcess.Nekudos),
            /*The only nekudos that can be the last character of a word are
            * Kumatz, Pasach and Shva. Vov-Cholam and Vov-Dagesh are added as end letters to prevent long-letter + vov-dagesh at end of word*/
            validEndNekudos = string.Join(" ", new char[] { (char)0x05b0, (char)0x05b7, (char)0x05b8 }),
            /* The letters מ,נ,צ,פ,כ are invalid as the last character of a word*/
            validEndLetters = "אבגדהוזחטיךלםןסעףץקרתבּוּוֹךּשׁשׂתּ",
            /* The alef and the yud are the only letters that are valid in the middle of a word without a nekuda following it;
             *     but not as the first letter of the word. */
            validStandaloneLetters = "יא",
            validLastNekudosTwoLetterWord = "וּוֹ";
            /* The vov-dagesh is the only nekuda that is valid by itself;
             *     but only as the first letter of a word.*/
            char melupum = 'וּ';

            return string.Format("{{0}} melupum = {0}\r\n{{1}} validMiddleLetters = {1}\r\n{{2}} validMiddleNekudos = {2}\r\n{{3}} validStandaloneLetters = {3}\r\n{{4}} validEndLetters = {4}\r\n{{5}} validEndNekudos = {5}\r\n{{6}} validLastNekudosTwoLetterWord = {6}",
                melupum, validMiddleLetters, validMiddleNekudos,
                validStandaloneLetters, validEndLetters, validEndNekudos, validLastNekudosTwoLetterWord);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            this.HideResults();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.HideResults();
        }

        private void llToggleResults_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if(this.lbInfo.Visible)
            {
                this.HideResults();
            }
            else
            {
                this.ShowResults();
            }
        }

        private void ShowResults()
        {
            this.lbInfo.Visible = true;
            this.lblResults.Text = "Word Information";
            this.llToggleResults.Text = "Show Placeholders";
        }

        private void HideResults()
        {
            this.lbInfo.Visible = false;
            this.lblResults.Text = "Available placeholders: (use numbers surrounded by curly braces)";
            this.llToggleResults.Text = "Show Results";
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.textBox2.Text = this._regExstring;
        }
    }
}
