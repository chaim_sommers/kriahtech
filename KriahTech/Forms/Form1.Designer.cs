﻿namespace KriahTech
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnGo = new System.Windows.Forms.Button();
            this.cbAlef = new System.Windows.Forms.CheckBox();
            this.cbBeis = new System.Windows.Forms.CheckBox();
            this.cbVeis = new System.Windows.Forms.CheckBox();
            this.cbGimmel = new System.Windows.Forms.CheckBox();
            this.cbDaled = new System.Windows.Forms.CheckBox();
            this.cbHei = new System.Windows.Forms.CheckBox();
            this.cbVav = new System.Windows.Forms.CheckBox();
            this.cbZayin = new System.Windows.Forms.CheckBox();
            this.cbChes = new System.Windows.Forms.CheckBox();
            this.cbTes = new System.Windows.Forms.CheckBox();
            this.cbYud = new System.Windows.Forms.CheckBox();
            this.cbKof = new System.Windows.Forms.CheckBox();
            this.cbChof = new System.Windows.Forms.CheckBox();
            this.cbEndKof = new System.Windows.Forms.CheckBox();
            this.cbEndChof = new System.Windows.Forms.CheckBox();
            this.cbLamed = new System.Windows.Forms.CheckBox();
            this.cbMem = new System.Windows.Forms.CheckBox();
            this.cbEndMem = new System.Windows.Forms.CheckBox();
            this.cbNun = new System.Windows.Forms.CheckBox();
            this.cbEndNun = new System.Windows.Forms.CheckBox();
            this.cbSamech = new System.Windows.Forms.CheckBox();
            this.cbAyin = new System.Windows.Forms.CheckBox();
            this.cbPay = new System.Windows.Forms.CheckBox();
            this.cbFay = new System.Windows.Forms.CheckBox();
            this.cbEndFay = new System.Windows.Forms.CheckBox();
            this.cbTzadik = new System.Windows.Forms.CheckBox();
            this.cbEndTzadik = new System.Windows.Forms.CheckBox();
            this.cbKuf = new System.Windows.Forms.CheckBox();
            this.cbReish = new System.Windows.Forms.CheckBox();
            this.cbShin = new System.Windows.Forms.CheckBox();
            this.cbSin = new System.Windows.Forms.CheckBox();
            this.cbTuf = new System.Windows.Forms.CheckBox();
            this.cbSuf = new System.Windows.Forms.CheckBox();
            this.cbKumatz = new System.Windows.Forms.CheckBox();
            this.cbPasach = new System.Windows.Forms.CheckBox();
            this.cbChirik = new System.Windows.Forms.CheckBox();
            this.cbSegol = new System.Windows.Forms.CheckBox();
            this.cbTzeirei = new System.Windows.Forms.CheckBox();
            this.cbShva = new System.Windows.Forms.CheckBox();
            this.cbCholam = new System.Windows.Forms.CheckBox();
            this.cbCholumVav = new System.Windows.Forms.CheckBox();
            this.cbKubutz = new System.Windows.Forms.CheckBox();
            this.cbMelupumBegining = new System.Windows.Forms.CheckBox();
            this.cbChatafKumatz = new System.Windows.Forms.CheckBox();
            this.cbChatafPasach = new System.Windows.Forms.CheckBox();
            this.cbChatafSegol = new System.Windows.Forms.CheckBox();
            this.pnlOptions = new System.Windows.Forms.Panel();
            this.llAdmin = new System.Windows.Forms.LinkLabel();
            this.btnSettings = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlNekudos = new System.Windows.Forms.Panel();
            this.btnSelectAllNekudos = new System.Windows.Forms.Button();
            this.btnDisallowAllNekudos = new System.Windows.Forms.Button();
            this.cbMelupumMiddle = new System.Windows.Forms.CheckBox();
            this.pnlLetters = new System.Windows.Forms.Panel();
            this.btnSelectAllletters = new System.Windows.Forms.Button();
            this.btnDisallowAllLetters = new System.Windows.Forms.Button();
            this.cbMapikHei = new System.Windows.Forms.CheckBox();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.pnlSpecialEndings = new System.Windows.Forms.Panel();
            this.btnSelectAllEndings = new System.Windows.Forms.Button();
            this.btnDisallowAllEndings = new System.Windows.Forms.Button();
            this.cbEndingKumatzYud = new System.Windows.Forms.CheckBox();
            this.cbEndingPasachYud = new System.Windows.Forms.CheckBox();
            this.cbEndingMelupumYud = new System.Windows.Forms.CheckBox();
            this.cbEndingKumatzYudVuv = new System.Windows.Forms.CheckBox();
            this.cbEndingChesPasach = new System.Windows.Forms.CheckBox();
            this.cbEndingHeiPasach = new System.Windows.Forms.CheckBox();
            this.cbEndingKumatzMapikHei = new System.Windows.Forms.CheckBox();
            this.btnToggleSearch = new System.Windows.Forms.Button();
            this.pnlResults = new System.Windows.Forms.Panel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.pnlAdvancedRules = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnGoAdvncFilter = new System.Windows.Forms.Button();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.btnGoAdvancedRules = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.tabControlSearch = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cbIncludeAdvancedFilter = new System.Windows.Forms.CheckBox();
            this.pnlOptions.SuspendLayout();
            this.pnlNekudos.SuspendLayout();
            this.pnlLetters.SuspendLayout();
            this.pnlSearch.SuspendLayout();
            this.pnlSpecialEndings.SuspendLayout();
            this.pnlResults.SuspendLayout();
            this.pnlAdvancedRules.SuspendLayout();
            this.tabControlSearch.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGo
            // 
            this.btnGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGo.Font = new System.Drawing.Font("Arial Black", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnGo.ForeColor = System.Drawing.Color.Maroon;
            this.btnGo.Location = new System.Drawing.Point(10, 10);
            this.btnGo.Margin = new System.Windows.Forms.Padding(2);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(170, 48);
            this.btnGo.TabIndex = 0;
            this.btnGo.Text = "Go";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // cbAlef
            // 
            this.cbAlef.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAlef.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbAlef.BackColor = System.Drawing.Color.Lavender;
            this.cbAlef.Checked = true;
            this.cbAlef.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAlef.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAlef.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbAlef.Location = new System.Drawing.Point(173, 16);
            this.cbAlef.Margin = new System.Windows.Forms.Padding(2);
            this.cbAlef.Name = "cbAlef";
            this.cbAlef.Size = new System.Drawing.Size(40, 41);
            this.cbAlef.TabIndex = 0;
            this.cbAlef.Text = "א";
            this.cbAlef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbAlef.ThreeState = true;
            this.cbAlef.UseVisualStyleBackColor = false;
            this.cbAlef.CheckedChanged += new System.EventHandler(this.cb_CheckedChanged);
            this.cbAlef.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbBeis
            // 
            this.cbBeis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBeis.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbBeis.BackColor = System.Drawing.Color.Lavender;
            this.cbBeis.Checked = true;
            this.cbBeis.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBeis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBeis.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbBeis.Location = new System.Drawing.Point(173, 62);
            this.cbBeis.Margin = new System.Windows.Forms.Padding(2);
            this.cbBeis.Name = "cbBeis";
            this.cbBeis.Size = new System.Drawing.Size(40, 41);
            this.cbBeis.TabIndex = 1;
            this.cbBeis.Text = "בּ";
            this.cbBeis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBeis.ThreeState = true;
            this.cbBeis.UseVisualStyleBackColor = false;
            this.cbBeis.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbVeis
            // 
            this.cbVeis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbVeis.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbVeis.BackColor = System.Drawing.Color.Lavender;
            this.cbVeis.Checked = true;
            this.cbVeis.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbVeis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVeis.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbVeis.Location = new System.Drawing.Point(173, 110);
            this.cbVeis.Margin = new System.Windows.Forms.Padding(2);
            this.cbVeis.Name = "cbVeis";
            this.cbVeis.Size = new System.Drawing.Size(40, 41);
            this.cbVeis.TabIndex = 2;
            this.cbVeis.Text = "ב";
            this.cbVeis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbVeis.ThreeState = true;
            this.cbVeis.UseVisualStyleBackColor = false;
            this.cbVeis.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbGimmel
            // 
            this.cbGimmel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGimmel.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbGimmel.BackColor = System.Drawing.Color.Lavender;
            this.cbGimmel.Checked = true;
            this.cbGimmel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGimmel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbGimmel.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbGimmel.Location = new System.Drawing.Point(173, 158);
            this.cbGimmel.Margin = new System.Windows.Forms.Padding(2);
            this.cbGimmel.Name = "cbGimmel";
            this.cbGimmel.Size = new System.Drawing.Size(40, 41);
            this.cbGimmel.TabIndex = 3;
            this.cbGimmel.Text = "ג";
            this.cbGimmel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbGimmel.ThreeState = true;
            this.cbGimmel.UseVisualStyleBackColor = false;
            this.cbGimmel.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbDaled
            // 
            this.cbDaled.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDaled.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbDaled.BackColor = System.Drawing.Color.Lavender;
            this.cbDaled.Checked = true;
            this.cbDaled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDaled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDaled.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbDaled.Location = new System.Drawing.Point(173, 206);
            this.cbDaled.Margin = new System.Windows.Forms.Padding(2);
            this.cbDaled.Name = "cbDaled";
            this.cbDaled.Size = new System.Drawing.Size(40, 41);
            this.cbDaled.TabIndex = 4;
            this.cbDaled.Text = "ד";
            this.cbDaled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbDaled.ThreeState = true;
            this.cbDaled.UseVisualStyleBackColor = false;
            this.cbDaled.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbHei
            // 
            this.cbHei.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbHei.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbHei.BackColor = System.Drawing.Color.Lavender;
            this.cbHei.Checked = true;
            this.cbHei.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbHei.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbHei.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbHei.Location = new System.Drawing.Point(173, 254);
            this.cbHei.Margin = new System.Windows.Forms.Padding(2);
            this.cbHei.Name = "cbHei";
            this.cbHei.Size = new System.Drawing.Size(40, 41);
            this.cbHei.TabIndex = 5;
            this.cbHei.Text = "ה";
            this.cbHei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbHei.ThreeState = true;
            this.cbHei.UseVisualStyleBackColor = false;
            this.cbHei.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbVav
            // 
            this.cbVav.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbVav.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbVav.BackColor = System.Drawing.Color.Lavender;
            this.cbVav.Checked = true;
            this.cbVav.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbVav.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVav.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbVav.Location = new System.Drawing.Point(173, 348);
            this.cbVav.Margin = new System.Windows.Forms.Padding(2);
            this.cbVav.Name = "cbVav";
            this.cbVav.Size = new System.Drawing.Size(40, 41);
            this.cbVav.TabIndex = 7;
            this.cbVav.Text = "ו";
            this.cbVav.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbVav.ThreeState = true;
            this.cbVav.UseVisualStyleBackColor = false;
            this.cbVav.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbZayin
            // 
            this.cbZayin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbZayin.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbZayin.BackColor = System.Drawing.Color.Lavender;
            this.cbZayin.Checked = true;
            this.cbZayin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbZayin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbZayin.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbZayin.Location = new System.Drawing.Point(173, 396);
            this.cbZayin.Margin = new System.Windows.Forms.Padding(2);
            this.cbZayin.Name = "cbZayin";
            this.cbZayin.Size = new System.Drawing.Size(40, 41);
            this.cbZayin.TabIndex = 8;
            this.cbZayin.Text = "ז";
            this.cbZayin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbZayin.ThreeState = true;
            this.cbZayin.UseVisualStyleBackColor = false;
            this.cbZayin.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbChes
            // 
            this.cbChes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChes.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbChes.BackColor = System.Drawing.Color.Lavender;
            this.cbChes.Checked = true;
            this.cbChes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbChes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbChes.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbChes.Location = new System.Drawing.Point(173, 444);
            this.cbChes.Margin = new System.Windows.Forms.Padding(2);
            this.cbChes.Name = "cbChes";
            this.cbChes.Size = new System.Drawing.Size(40, 41);
            this.cbChes.TabIndex = 9;
            this.cbChes.Text = "ח";
            this.cbChes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbChes.ThreeState = true;
            this.cbChes.UseVisualStyleBackColor = false;
            this.cbChes.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbTes
            // 
            this.cbTes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTes.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbTes.BackColor = System.Drawing.Color.Lavender;
            this.cbTes.Checked = true;
            this.cbTes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTes.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbTes.Location = new System.Drawing.Point(122, 16);
            this.cbTes.Margin = new System.Windows.Forms.Padding(2);
            this.cbTes.Name = "cbTes";
            this.cbTes.Size = new System.Drawing.Size(40, 41);
            this.cbTes.TabIndex = 10;
            this.cbTes.Text = "ט";
            this.cbTes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbTes.ThreeState = true;
            this.cbTes.UseVisualStyleBackColor = false;
            this.cbTes.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbYud
            // 
            this.cbYud.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbYud.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbYud.BackColor = System.Drawing.Color.Lavender;
            this.cbYud.Checked = true;
            this.cbYud.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbYud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbYud.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbYud.Location = new System.Drawing.Point(122, 62);
            this.cbYud.Margin = new System.Windows.Forms.Padding(2);
            this.cbYud.Name = "cbYud";
            this.cbYud.Size = new System.Drawing.Size(40, 41);
            this.cbYud.TabIndex = 11;
            this.cbYud.Text = "י";
            this.cbYud.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbYud.ThreeState = true;
            this.cbYud.UseVisualStyleBackColor = false;
            this.cbYud.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbKof
            // 
            this.cbKof.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbKof.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbKof.BackColor = System.Drawing.Color.Lavender;
            this.cbKof.Checked = true;
            this.cbKof.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbKof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbKof.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbKof.Location = new System.Drawing.Point(122, 109);
            this.cbKof.Margin = new System.Windows.Forms.Padding(2);
            this.cbKof.Name = "cbKof";
            this.cbKof.Size = new System.Drawing.Size(40, 41);
            this.cbKof.TabIndex = 12;
            this.cbKof.Text = "כּ";
            this.cbKof.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbKof.ThreeState = true;
            this.cbKof.UseVisualStyleBackColor = false;
            this.cbKof.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbChof
            // 
            this.cbChof.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChof.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbChof.BackColor = System.Drawing.Color.Lavender;
            this.cbChof.Checked = true;
            this.cbChof.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbChof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbChof.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbChof.Location = new System.Drawing.Point(122, 157);
            this.cbChof.Margin = new System.Windows.Forms.Padding(2);
            this.cbChof.Name = "cbChof";
            this.cbChof.Size = new System.Drawing.Size(40, 41);
            this.cbChof.TabIndex = 13;
            this.cbChof.Text = "כ";
            this.cbChof.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbChof.ThreeState = true;
            this.cbChof.UseVisualStyleBackColor = false;
            this.cbChof.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbEndKof
            // 
            this.cbEndKof.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEndKof.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndKof.BackColor = System.Drawing.Color.Lavender;
            this.cbEndKof.Checked = true;
            this.cbEndKof.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndKof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndKof.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndKof.Location = new System.Drawing.Point(122, 205);
            this.cbEndKof.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndKof.Name = "cbEndKof";
            this.cbEndKof.Size = new System.Drawing.Size(40, 41);
            this.cbEndKof.TabIndex = 14;
            this.cbEndKof.Text = "ךּ";
            this.cbEndKof.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndKof.ThreeState = true;
            this.cbEndKof.UseVisualStyleBackColor = false;
            this.cbEndKof.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbEndChof
            // 
            this.cbEndChof.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEndChof.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndChof.BackColor = System.Drawing.Color.Lavender;
            this.cbEndChof.Checked = true;
            this.cbEndChof.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndChof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndChof.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndChof.Location = new System.Drawing.Point(122, 253);
            this.cbEndChof.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndChof.Name = "cbEndChof";
            this.cbEndChof.Size = new System.Drawing.Size(40, 41);
            this.cbEndChof.TabIndex = 15;
            this.cbEndChof.Text = "ך";
            this.cbEndChof.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndChof.ThreeState = true;
            this.cbEndChof.UseVisualStyleBackColor = false;
            this.cbEndChof.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbLamed
            // 
            this.cbLamed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbLamed.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbLamed.BackColor = System.Drawing.Color.Lavender;
            this.cbLamed.Checked = true;
            this.cbLamed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLamed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLamed.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbLamed.Location = new System.Drawing.Point(122, 300);
            this.cbLamed.Margin = new System.Windows.Forms.Padding(2);
            this.cbLamed.Name = "cbLamed";
            this.cbLamed.Size = new System.Drawing.Size(40, 41);
            this.cbLamed.TabIndex = 16;
            this.cbLamed.Text = "ל";
            this.cbLamed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbLamed.ThreeState = true;
            this.cbLamed.UseVisualStyleBackColor = false;
            this.cbLamed.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbMem
            // 
            this.cbMem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMem.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbMem.BackColor = System.Drawing.Color.Lavender;
            this.cbMem.Checked = true;
            this.cbMem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbMem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMem.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbMem.Location = new System.Drawing.Point(122, 348);
            this.cbMem.Margin = new System.Windows.Forms.Padding(2);
            this.cbMem.Name = "cbMem";
            this.cbMem.Size = new System.Drawing.Size(40, 41);
            this.cbMem.TabIndex = 17;
            this.cbMem.Text = "מ";
            this.cbMem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbMem.ThreeState = true;
            this.cbMem.UseVisualStyleBackColor = false;
            this.cbMem.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbEndMem
            // 
            this.cbEndMem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEndMem.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndMem.BackColor = System.Drawing.Color.Lavender;
            this.cbEndMem.Checked = true;
            this.cbEndMem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndMem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndMem.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndMem.Location = new System.Drawing.Point(122, 396);
            this.cbEndMem.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndMem.Name = "cbEndMem";
            this.cbEndMem.Size = new System.Drawing.Size(40, 41);
            this.cbEndMem.TabIndex = 18;
            this.cbEndMem.Text = "ם";
            this.cbEndMem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndMem.ThreeState = true;
            this.cbEndMem.UseVisualStyleBackColor = false;
            this.cbEndMem.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbNun
            // 
            this.cbNun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNun.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbNun.BackColor = System.Drawing.Color.Lavender;
            this.cbNun.Checked = true;
            this.cbNun.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbNun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbNun.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbNun.Location = new System.Drawing.Point(122, 444);
            this.cbNun.Margin = new System.Windows.Forms.Padding(2);
            this.cbNun.Name = "cbNun";
            this.cbNun.Size = new System.Drawing.Size(40, 41);
            this.cbNun.TabIndex = 19;
            this.cbNun.Text = "נ";
            this.cbNun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbNun.ThreeState = true;
            this.cbNun.UseVisualStyleBackColor = false;
            this.cbNun.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbEndNun
            // 
            this.cbEndNun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEndNun.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndNun.BackColor = System.Drawing.Color.Lavender;
            this.cbEndNun.Checked = true;
            this.cbEndNun.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndNun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndNun.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndNun.Location = new System.Drawing.Point(75, 16);
            this.cbEndNun.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndNun.Name = "cbEndNun";
            this.cbEndNun.Size = new System.Drawing.Size(40, 41);
            this.cbEndNun.TabIndex = 20;
            this.cbEndNun.Text = "ן";
            this.cbEndNun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndNun.ThreeState = true;
            this.cbEndNun.UseVisualStyleBackColor = false;
            this.cbEndNun.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbSamech
            // 
            this.cbSamech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSamech.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbSamech.BackColor = System.Drawing.Color.Lavender;
            this.cbSamech.Checked = true;
            this.cbSamech.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSamech.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSamech.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbSamech.Location = new System.Drawing.Point(75, 62);
            this.cbSamech.Margin = new System.Windows.Forms.Padding(2);
            this.cbSamech.Name = "cbSamech";
            this.cbSamech.Size = new System.Drawing.Size(40, 41);
            this.cbSamech.TabIndex = 21;
            this.cbSamech.Text = "ס";
            this.cbSamech.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbSamech.ThreeState = true;
            this.cbSamech.UseVisualStyleBackColor = false;
            this.cbSamech.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbAyin
            // 
            this.cbAyin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAyin.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbAyin.BackColor = System.Drawing.Color.Lavender;
            this.cbAyin.Checked = true;
            this.cbAyin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAyin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAyin.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbAyin.Location = new System.Drawing.Point(75, 109);
            this.cbAyin.Margin = new System.Windows.Forms.Padding(2);
            this.cbAyin.Name = "cbAyin";
            this.cbAyin.Size = new System.Drawing.Size(40, 41);
            this.cbAyin.TabIndex = 22;
            this.cbAyin.Text = "ע";
            this.cbAyin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbAyin.ThreeState = true;
            this.cbAyin.UseVisualStyleBackColor = false;
            this.cbAyin.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbPay
            // 
            this.cbPay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPay.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbPay.BackColor = System.Drawing.Color.Lavender;
            this.cbPay.Checked = true;
            this.cbPay.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPay.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbPay.Location = new System.Drawing.Point(75, 157);
            this.cbPay.Margin = new System.Windows.Forms.Padding(2);
            this.cbPay.Name = "cbPay";
            this.cbPay.Size = new System.Drawing.Size(40, 41);
            this.cbPay.TabIndex = 23;
            this.cbPay.Text = "פּ";
            this.cbPay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbPay.ThreeState = true;
            this.cbPay.UseVisualStyleBackColor = false;
            this.cbPay.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbFay
            // 
            this.cbFay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFay.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbFay.BackColor = System.Drawing.Color.Lavender;
            this.cbFay.Checked = true;
            this.cbFay.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbFay.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbFay.Location = new System.Drawing.Point(75, 205);
            this.cbFay.Margin = new System.Windows.Forms.Padding(2);
            this.cbFay.Name = "cbFay";
            this.cbFay.Size = new System.Drawing.Size(40, 41);
            this.cbFay.TabIndex = 24;
            this.cbFay.Text = "פ";
            this.cbFay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbFay.ThreeState = true;
            this.cbFay.UseVisualStyleBackColor = false;
            this.cbFay.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbEndFay
            // 
            this.cbEndFay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEndFay.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndFay.BackColor = System.Drawing.Color.Lavender;
            this.cbEndFay.Checked = true;
            this.cbEndFay.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndFay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndFay.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndFay.Location = new System.Drawing.Point(75, 254);
            this.cbEndFay.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndFay.Name = "cbEndFay";
            this.cbEndFay.Size = new System.Drawing.Size(40, 41);
            this.cbEndFay.TabIndex = 26;
            this.cbEndFay.Text = "ף";
            this.cbEndFay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndFay.ThreeState = true;
            this.cbEndFay.UseVisualStyleBackColor = false;
            this.cbEndFay.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbTzadik
            // 
            this.cbTzadik.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTzadik.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbTzadik.BackColor = System.Drawing.Color.Lavender;
            this.cbTzadik.Checked = true;
            this.cbTzadik.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTzadik.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTzadik.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbTzadik.Location = new System.Drawing.Point(75, 302);
            this.cbTzadik.Margin = new System.Windows.Forms.Padding(2);
            this.cbTzadik.Name = "cbTzadik";
            this.cbTzadik.Size = new System.Drawing.Size(40, 41);
            this.cbTzadik.TabIndex = 27;
            this.cbTzadik.Text = "צ";
            this.cbTzadik.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbTzadik.ThreeState = true;
            this.cbTzadik.UseVisualStyleBackColor = false;
            this.cbTzadik.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbEndTzadik
            // 
            this.cbEndTzadik.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEndTzadik.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndTzadik.BackColor = System.Drawing.Color.Lavender;
            this.cbEndTzadik.Checked = true;
            this.cbEndTzadik.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndTzadik.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndTzadik.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndTzadik.Location = new System.Drawing.Point(75, 350);
            this.cbEndTzadik.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndTzadik.Name = "cbEndTzadik";
            this.cbEndTzadik.Size = new System.Drawing.Size(40, 41);
            this.cbEndTzadik.TabIndex = 28;
            this.cbEndTzadik.Text = "ץ";
            this.cbEndTzadik.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndTzadik.ThreeState = true;
            this.cbEndTzadik.UseVisualStyleBackColor = false;
            this.cbEndTzadik.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbKuf
            // 
            this.cbKuf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbKuf.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbKuf.BackColor = System.Drawing.Color.Lavender;
            this.cbKuf.Checked = true;
            this.cbKuf.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbKuf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbKuf.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbKuf.Location = new System.Drawing.Point(75, 398);
            this.cbKuf.Margin = new System.Windows.Forms.Padding(2);
            this.cbKuf.Name = "cbKuf";
            this.cbKuf.Size = new System.Drawing.Size(40, 41);
            this.cbKuf.TabIndex = 29;
            this.cbKuf.Text = "ק";
            this.cbKuf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbKuf.ThreeState = true;
            this.cbKuf.UseVisualStyleBackColor = false;
            this.cbKuf.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbReish
            // 
            this.cbReish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbReish.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbReish.BackColor = System.Drawing.Color.Lavender;
            this.cbReish.Checked = true;
            this.cbReish.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbReish.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbReish.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbReish.Location = new System.Drawing.Point(75, 444);
            this.cbReish.Margin = new System.Windows.Forms.Padding(2);
            this.cbReish.Name = "cbReish";
            this.cbReish.Size = new System.Drawing.Size(40, 41);
            this.cbReish.TabIndex = 30;
            this.cbReish.Text = "ר";
            this.cbReish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbReish.ThreeState = true;
            this.cbReish.UseVisualStyleBackColor = false;
            this.cbReish.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbShin
            // 
            this.cbShin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbShin.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShin.BackColor = System.Drawing.Color.Lavender;
            this.cbShin.Checked = true;
            this.cbShin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShin.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbShin.Location = new System.Drawing.Point(15, 16);
            this.cbShin.Margin = new System.Windows.Forms.Padding(2);
            this.cbShin.Name = "cbShin";
            this.cbShin.Size = new System.Drawing.Size(50, 41);
            this.cbShin.TabIndex = 31;
            this.cbShin.Text = "שׁ";
            this.cbShin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbShin.ThreeState = true;
            this.cbShin.UseVisualStyleBackColor = false;
            this.cbShin.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbSin
            // 
            this.cbSin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSin.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbSin.BackColor = System.Drawing.Color.Lavender;
            this.cbSin.Checked = true;
            this.cbSin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSin.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbSin.Location = new System.Drawing.Point(15, 63);
            this.cbSin.Margin = new System.Windows.Forms.Padding(2);
            this.cbSin.Name = "cbSin";
            this.cbSin.Size = new System.Drawing.Size(50, 41);
            this.cbSin.TabIndex = 32;
            this.cbSin.Text = "שׂ";
            this.cbSin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbSin.ThreeState = true;
            this.cbSin.UseVisualStyleBackColor = false;
            this.cbSin.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbTuf
            // 
            this.cbTuf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTuf.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbTuf.BackColor = System.Drawing.Color.Lavender;
            this.cbTuf.Checked = true;
            this.cbTuf.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTuf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTuf.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbTuf.Location = new System.Drawing.Point(15, 111);
            this.cbTuf.Margin = new System.Windows.Forms.Padding(2);
            this.cbTuf.Name = "cbTuf";
            this.cbTuf.Size = new System.Drawing.Size(50, 41);
            this.cbTuf.TabIndex = 33;
            this.cbTuf.Text = "תּ";
            this.cbTuf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbTuf.ThreeState = true;
            this.cbTuf.UseVisualStyleBackColor = false;
            this.cbTuf.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbSuf
            // 
            this.cbSuf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSuf.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbSuf.BackColor = System.Drawing.Color.Lavender;
            this.cbSuf.Checked = true;
            this.cbSuf.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSuf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSuf.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbSuf.Location = new System.Drawing.Point(15, 159);
            this.cbSuf.Margin = new System.Windows.Forms.Padding(2);
            this.cbSuf.Name = "cbSuf";
            this.cbSuf.Size = new System.Drawing.Size(50, 41);
            this.cbSuf.TabIndex = 34;
            this.cbSuf.Text = "ת";
            this.cbSuf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbSuf.ThreeState = true;
            this.cbSuf.UseVisualStyleBackColor = false;
            this.cbSuf.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbKumatz
            // 
            this.cbKumatz.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbKumatz.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbKumatz.BackColor = System.Drawing.Color.Lavender;
            this.cbKumatz.Checked = true;
            this.cbKumatz.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbKumatz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbKumatz.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbKumatz.Location = new System.Drawing.Point(135, 14);
            this.cbKumatz.Margin = new System.Windows.Forms.Padding(2);
            this.cbKumatz.Name = "cbKumatz";
            this.cbKumatz.Size = new System.Drawing.Size(100, 80);
            this.cbKumatz.TabIndex = 0;
            this.cbKumatz.Text = " ָ";
            this.cbKumatz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbKumatz.ThreeState = true;
            this.cbKumatz.UseVisualStyleBackColor = false;
            this.cbKumatz.CheckStateChanged += new System.EventHandler(this.cbKumatz_CheckStateChanged);
            // 
            // cbPasach
            // 
            this.cbPasach.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbPasach.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbPasach.BackColor = System.Drawing.Color.Lavender;
            this.cbPasach.Checked = true;
            this.cbPasach.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPasach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPasach.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbPasach.Location = new System.Drawing.Point(135, 96);
            this.cbPasach.Margin = new System.Windows.Forms.Padding(2);
            this.cbPasach.Name = "cbPasach";
            this.cbPasach.Size = new System.Drawing.Size(100, 80);
            this.cbPasach.TabIndex = 1;
            this.cbPasach.Text = " ַ";
            this.cbPasach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbPasach.ThreeState = true;
            this.cbPasach.UseVisualStyleBackColor = false;
            this.cbPasach.CheckStateChanged += new System.EventHandler(this.cbPasach_CheckStateChanged);
            // 
            // cbChirik
            // 
            this.cbChirik.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbChirik.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbChirik.BackColor = System.Drawing.Color.Lavender;
            this.cbChirik.Checked = true;
            this.cbChirik.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbChirik.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbChirik.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbChirik.Location = new System.Drawing.Point(135, 178);
            this.cbChirik.Margin = new System.Windows.Forms.Padding(2);
            this.cbChirik.Name = "cbChirik";
            this.cbChirik.Size = new System.Drawing.Size(100, 80);
            this.cbChirik.TabIndex = 2;
            this.cbChirik.Text = " ִ";
            this.cbChirik.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbChirik.ThreeState = true;
            this.cbChirik.UseVisualStyleBackColor = false;
            this.cbChirik.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbSegol
            // 
            this.cbSegol.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbSegol.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbSegol.BackColor = System.Drawing.Color.Lavender;
            this.cbSegol.Checked = true;
            this.cbSegol.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSegol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSegol.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbSegol.Location = new System.Drawing.Point(135, 260);
            this.cbSegol.Margin = new System.Windows.Forms.Padding(2);
            this.cbSegol.Name = "cbSegol";
            this.cbSegol.Size = new System.Drawing.Size(100, 80);
            this.cbSegol.TabIndex = 3;
            this.cbSegol.Text = " ֶ";
            this.cbSegol.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbSegol.ThreeState = true;
            this.cbSegol.UseVisualStyleBackColor = false;
            this.cbSegol.CheckStateChanged += new System.EventHandler(this.cbSegol_CheckStateChanged);
            // 
            // cbTzeirei
            // 
            this.cbTzeirei.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbTzeirei.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbTzeirei.BackColor = System.Drawing.Color.Lavender;
            this.cbTzeirei.Checked = true;
            this.cbTzeirei.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTzeirei.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTzeirei.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbTzeirei.Location = new System.Drawing.Point(135, 342);
            this.cbTzeirei.Margin = new System.Windows.Forms.Padding(2);
            this.cbTzeirei.Name = "cbTzeirei";
            this.cbTzeirei.Size = new System.Drawing.Size(100, 80);
            this.cbTzeirei.TabIndex = 4;
            this.cbTzeirei.Text = " ֵ";
            this.cbTzeirei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbTzeirei.ThreeState = true;
            this.cbTzeirei.UseVisualStyleBackColor = false;
            this.cbTzeirei.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbShva
            // 
            this.cbShva.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbShva.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShva.BackColor = System.Drawing.Color.Lavender;
            this.cbShva.Checked = true;
            this.cbShva.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShva.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShva.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbShva.Location = new System.Drawing.Point(135, 424);
            this.cbShva.Margin = new System.Windows.Forms.Padding(2);
            this.cbShva.Name = "cbShva";
            this.cbShva.Size = new System.Drawing.Size(100, 80);
            this.cbShva.TabIndex = 5;
            this.cbShva.Text = " ְ";
            this.cbShva.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbShva.ThreeState = true;
            this.cbShva.UseVisualStyleBackColor = false;
            this.cbShva.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbCholam
            // 
            this.cbCholam.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbCholam.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbCholam.BackColor = System.Drawing.Color.Lavender;
            this.cbCholam.Checked = true;
            this.cbCholam.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCholam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCholam.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbCholam.Location = new System.Drawing.Point(135, 506);
            this.cbCholam.Margin = new System.Windows.Forms.Padding(2);
            this.cbCholam.Name = "cbCholam";
            this.cbCholam.Size = new System.Drawing.Size(100, 80);
            this.cbCholam.TabIndex = 6;
            this.cbCholam.Text = " ֹ";
            this.cbCholam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbCholam.ThreeState = true;
            this.cbCholam.UseVisualStyleBackColor = false;
            this.cbCholam.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbCholumVav
            // 
            this.cbCholumVav.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbCholumVav.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbCholumVav.BackColor = System.Drawing.Color.Lavender;
            this.cbCholumVav.Checked = true;
            this.cbCholumVav.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCholumVav.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCholumVav.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbCholumVav.Location = new System.Drawing.Point(30, 14);
            this.cbCholumVav.Margin = new System.Windows.Forms.Padding(2);
            this.cbCholumVav.Name = "cbCholumVav";
            this.cbCholumVav.Size = new System.Drawing.Size(100, 80);
            this.cbCholumVav.TabIndex = 7;
            this.cbCholumVav.Text = "וֹ";
            this.cbCholumVav.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbCholumVav.ThreeState = true;
            this.cbCholumVav.UseVisualStyleBackColor = false;
            this.cbCholumVav.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbKubutz
            // 
            this.cbKubutz.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbKubutz.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbKubutz.BackColor = System.Drawing.Color.Lavender;
            this.cbKubutz.Checked = true;
            this.cbKubutz.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbKubutz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbKubutz.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbKubutz.Location = new System.Drawing.Point(30, 96);
            this.cbKubutz.Margin = new System.Windows.Forms.Padding(2);
            this.cbKubutz.Name = "cbKubutz";
            this.cbKubutz.Size = new System.Drawing.Size(100, 80);
            this.cbKubutz.TabIndex = 8;
            this.cbKubutz.Text = " ֻ";
            this.cbKubutz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbKubutz.ThreeState = true;
            this.cbKubutz.UseVisualStyleBackColor = false;
            this.cbKubutz.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbMelupumBegining
            // 
            this.cbMelupumBegining.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbMelupumBegining.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbMelupumBegining.BackColor = System.Drawing.Color.Lavender;
            this.cbMelupumBegining.Checked = true;
            this.cbMelupumBegining.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbMelupumBegining.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMelupumBegining.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbMelupumBegining.Location = new System.Drawing.Point(30, 178);
            this.cbMelupumBegining.Margin = new System.Windows.Forms.Padding(2);
            this.cbMelupumBegining.Name = "cbMelupumBegining";
            this.cbMelupumBegining.Size = new System.Drawing.Size(100, 80);
            this.cbMelupumBegining.TabIndex = 9;
            this.cbMelupumBegining.Text = "..וּ";
            this.cbMelupumBegining.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbMelupumBegining.ThreeState = true;
            this.toolTip1.SetToolTip(this.cbMelupumBegining, "Melupum in the begining of the word");
            this.cbMelupumBegining.UseVisualStyleBackColor = false;
            this.cbMelupumBegining.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbChatafKumatz
            // 
            this.cbChatafKumatz.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbChatafKumatz.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbChatafKumatz.BackColor = System.Drawing.Color.Lavender;
            this.cbChatafKumatz.Checked = true;
            this.cbChatafKumatz.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbChatafKumatz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbChatafKumatz.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbChatafKumatz.Location = new System.Drawing.Point(30, 342);
            this.cbChatafKumatz.Margin = new System.Windows.Forms.Padding(2);
            this.cbChatafKumatz.Name = "cbChatafKumatz";
            this.cbChatafKumatz.Size = new System.Drawing.Size(100, 80);
            this.cbChatafKumatz.TabIndex = 10;
            this.cbChatafKumatz.Text = " ֳ";
            this.cbChatafKumatz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbChatafKumatz.ThreeState = true;
            this.cbChatafKumatz.UseVisualStyleBackColor = false;
            this.cbChatafKumatz.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbChatafPasach
            // 
            this.cbChatafPasach.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbChatafPasach.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbChatafPasach.BackColor = System.Drawing.Color.Lavender;
            this.cbChatafPasach.Checked = true;
            this.cbChatafPasach.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbChatafPasach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbChatafPasach.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbChatafPasach.Location = new System.Drawing.Point(30, 424);
            this.cbChatafPasach.Margin = new System.Windows.Forms.Padding(2);
            this.cbChatafPasach.Name = "cbChatafPasach";
            this.cbChatafPasach.Size = new System.Drawing.Size(100, 80);
            this.cbChatafPasach.TabIndex = 11;
            this.cbChatafPasach.Text = " ֲ";
            this.cbChatafPasach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbChatafPasach.ThreeState = true;
            this.cbChatafPasach.UseVisualStyleBackColor = false;
            this.cbChatafPasach.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // cbChatafSegol
            // 
            this.cbChatafSegol.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbChatafSegol.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbChatafSegol.BackColor = System.Drawing.Color.Lavender;
            this.cbChatafSegol.Checked = true;
            this.cbChatafSegol.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbChatafSegol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbChatafSegol.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbChatafSegol.Location = new System.Drawing.Point(30, 506);
            this.cbChatafSegol.Margin = new System.Windows.Forms.Padding(2);
            this.cbChatafSegol.Name = "cbChatafSegol";
            this.cbChatafSegol.Size = new System.Drawing.Size(100, 80);
            this.cbChatafSegol.TabIndex = 12;
            this.cbChatafSegol.Text = " ֱ";
            this.cbChatafSegol.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbChatafSegol.ThreeState = true;
            this.cbChatafSegol.UseVisualStyleBackColor = false;
            this.cbChatafSegol.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // pnlOptions
            // 
            this.pnlOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlOptions.BackColor = System.Drawing.Color.Lavender;
            this.pnlOptions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOptions.Controls.Add(this.cbIncludeAdvancedFilter);
            this.pnlOptions.Controls.Add(this.llAdmin);
            this.pnlOptions.Controls.Add(this.btnSettings);
            this.pnlOptions.Controls.Add(this.btnGo);
            this.pnlOptions.Controls.Add(this.label2);
            this.pnlOptions.Controls.Add(this.label1);
            this.pnlOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.pnlOptions.Location = new System.Drawing.Point(-3, 622);
            this.pnlOptions.Margin = new System.Windows.Forms.Padding(2);
            this.pnlOptions.Name = "pnlOptions";
            this.pnlOptions.Size = new System.Drawing.Size(673, 90);
            this.pnlOptions.TabIndex = 3;
            // 
            // llAdmin
            // 
            this.llAdmin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llAdmin.AutoSize = true;
            this.llAdmin.BackColor = System.Drawing.Color.GhostWhite;
            this.llAdmin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.llAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.llAdmin.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.llAdmin.Location = new System.Drawing.Point(570, 25);
            this.llAdmin.Name = "llAdmin";
            this.llAdmin.Size = new System.Drawing.Size(47, 17);
            this.llAdmin.TabIndex = 17;
            this.llAdmin.TabStop = true;
            this.llAdmin.Text = "Admin";
            this.llAdmin.Visible = false;
            this.llAdmin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llAdmin_LinkClicked);
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.BackColor = System.Drawing.Color.GhostWhite;
            this.btnSettings.BackgroundImage = global::KriahTech.Properties.Resources.SettingsLarger;
            this.btnSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSettings.FlatAppearance.BorderColor = System.Drawing.Color.GhostWhite;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Location = new System.Drawing.Point(614, 14);
            this.btnSettings.Margin = new System.Windows.Forms.Padding(2);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(42, 36);
            this.btnSettings.TabIndex = 16;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(6, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Loading word list.... ";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.GhostWhite;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.ForeColor = System.Drawing.Color.SlateGray;
            this.label1.Location = new System.Drawing.Point(190, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.label1.Size = new System.Drawing.Size(469, 75);
            this.label1.TabIndex = 2;
            this.label1.Text = "LIGHT BLUE: the word is allowed to contain this character.\r\nLIGHT GREEN:  the wor" +
    "d MUST contain this character.\r\nLIGHT RED: the word MUST NOT contain this charac" +
    "ter.";
            // 
            // pnlNekudos
            // 
            this.pnlNekudos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlNekudos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNekudos.Controls.Add(this.btnSelectAllNekudos);
            this.pnlNekudos.Controls.Add(this.btnDisallowAllNekudos);
            this.pnlNekudos.Controls.Add(this.cbKumatz);
            this.pnlNekudos.Controls.Add(this.cbPasach);
            this.pnlNekudos.Controls.Add(this.cbChatafSegol);
            this.pnlNekudos.Controls.Add(this.cbChirik);
            this.pnlNekudos.Controls.Add(this.cbChatafPasach);
            this.pnlNekudos.Controls.Add(this.cbSegol);
            this.pnlNekudos.Controls.Add(this.cbChatafKumatz);
            this.pnlNekudos.Controls.Add(this.cbTzeirei);
            this.pnlNekudos.Controls.Add(this.cbMelupumMiddle);
            this.pnlNekudos.Controls.Add(this.cbMelupumBegining);
            this.pnlNekudos.Controls.Add(this.cbShva);
            this.pnlNekudos.Controls.Add(this.cbKubutz);
            this.pnlNekudos.Controls.Add(this.cbCholam);
            this.pnlNekudos.Controls.Add(this.cbCholumVav);
            this.pnlNekudos.Location = new System.Drawing.Point(183, -6);
            this.pnlNekudos.Margin = new System.Windows.Forms.Padding(2);
            this.pnlNekudos.Name = "pnlNekudos";
            this.pnlNekudos.Size = new System.Drawing.Size(261, 629);
            this.pnlNekudos.TabIndex = 1;
            // 
            // btnSelectAllNekudos
            // 
            this.btnSelectAllNekudos.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSelectAllNekudos.BackColor = System.Drawing.Color.Transparent;
            this.btnSelectAllNekudos.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnSelectAllNekudos.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSelectAllNekudos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectAllNekudos.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnSelectAllNekudos.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnSelectAllNekudos.Location = new System.Drawing.Point(14, 598);
            this.btnSelectAllNekudos.Name = "btnSelectAllNekudos";
            this.btnSelectAllNekudos.Size = new System.Drawing.Size(113, 28);
            this.btnSelectAllNekudos.TabIndex = 13;
            this.btnSelectAllNekudos.TabStop = false;
            this.btnSelectAllNekudos.Text = "Allow All";
            this.btnSelectAllNekudos.UseVisualStyleBackColor = false;
            this.btnSelectAllNekudos.Click += new System.EventHandler(this.btnSelectAllNekudos_Click);
            // 
            // btnDisallowAllNekudos
            // 
            this.btnDisallowAllNekudos.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDisallowAllNekudos.BackColor = System.Drawing.Color.Transparent;
            this.btnDisallowAllNekudos.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnDisallowAllNekudos.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDisallowAllNekudos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisallowAllNekudos.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnDisallowAllNekudos.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnDisallowAllNekudos.Location = new System.Drawing.Point(132, 598);
            this.btnDisallowAllNekudos.Name = "btnDisallowAllNekudos";
            this.btnDisallowAllNekudos.Size = new System.Drawing.Size(113, 28);
            this.btnDisallowAllNekudos.TabIndex = 13;
            this.btnDisallowAllNekudos.TabStop = false;
            this.btnDisallowAllNekudos.Text = "Disallow All";
            this.btnDisallowAllNekudos.UseVisualStyleBackColor = false;
            this.btnDisallowAllNekudos.Click += new System.EventHandler(this.btnClearNekudos_Click);
            // 
            // cbMelupumMiddle
            // 
            this.cbMelupumMiddle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbMelupumMiddle.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbMelupumMiddle.BackColor = System.Drawing.Color.Lavender;
            this.cbMelupumMiddle.Checked = true;
            this.cbMelupumMiddle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbMelupumMiddle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMelupumMiddle.Font = new System.Drawing.Font("Times New Roman", 37F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbMelupumMiddle.Location = new System.Drawing.Point(30, 260);
            this.cbMelupumMiddle.Margin = new System.Windows.Forms.Padding(2);
            this.cbMelupumMiddle.Name = "cbMelupumMiddle";
            this.cbMelupumMiddle.Size = new System.Drawing.Size(100, 80);
            this.cbMelupumMiddle.TabIndex = 9;
            this.cbMelupumMiddle.Text = ".וּ..";
            this.cbMelupumMiddle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbMelupumMiddle.ThreeState = true;
            this.toolTip1.SetToolTip(this.cbMelupumMiddle, "Melupum in the middle or at the end of the word");
            this.cbMelupumMiddle.UseVisualStyleBackColor = false;
            this.cbMelupumMiddle.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // pnlLetters
            // 
            this.pnlLetters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlLetters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLetters.Controls.Add(this.btnSelectAllletters);
            this.pnlLetters.Controls.Add(this.btnDisallowAllLetters);
            this.pnlLetters.Controls.Add(this.cbMapikHei);
            this.pnlLetters.Controls.Add(this.cbAlef);
            this.pnlLetters.Controls.Add(this.cbBeis);
            this.pnlLetters.Controls.Add(this.cbSuf);
            this.pnlLetters.Controls.Add(this.cbVeis);
            this.pnlLetters.Controls.Add(this.cbTuf);
            this.pnlLetters.Controls.Add(this.cbGimmel);
            this.pnlLetters.Controls.Add(this.cbSin);
            this.pnlLetters.Controls.Add(this.cbDaled);
            this.pnlLetters.Controls.Add(this.cbShin);
            this.pnlLetters.Controls.Add(this.cbHei);
            this.pnlLetters.Controls.Add(this.cbReish);
            this.pnlLetters.Controls.Add(this.cbVav);
            this.pnlLetters.Controls.Add(this.cbKuf);
            this.pnlLetters.Controls.Add(this.cbZayin);
            this.pnlLetters.Controls.Add(this.cbEndTzadik);
            this.pnlLetters.Controls.Add(this.cbChes);
            this.pnlLetters.Controls.Add(this.cbTzadik);
            this.pnlLetters.Controls.Add(this.cbTes);
            this.pnlLetters.Controls.Add(this.cbEndFay);
            this.pnlLetters.Controls.Add(this.cbYud);
            this.pnlLetters.Controls.Add(this.cbKof);
            this.pnlLetters.Controls.Add(this.cbFay);
            this.pnlLetters.Controls.Add(this.cbChof);
            this.pnlLetters.Controls.Add(this.cbPay);
            this.pnlLetters.Controls.Add(this.cbEndKof);
            this.pnlLetters.Controls.Add(this.cbAyin);
            this.pnlLetters.Controls.Add(this.cbEndChof);
            this.pnlLetters.Controls.Add(this.cbSamech);
            this.pnlLetters.Controls.Add(this.cbLamed);
            this.pnlLetters.Controls.Add(this.cbEndNun);
            this.pnlLetters.Controls.Add(this.cbMem);
            this.pnlLetters.Controls.Add(this.cbNun);
            this.pnlLetters.Controls.Add(this.cbEndMem);
            this.pnlLetters.Location = new System.Drawing.Point(433, -6);
            this.pnlLetters.Margin = new System.Windows.Forms.Padding(2);
            this.pnlLetters.Name = "pnlLetters";
            this.pnlLetters.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pnlLetters.Size = new System.Drawing.Size(239, 629);
            this.pnlLetters.TabIndex = 0;
            // 
            // btnSelectAllletters
            // 
            this.btnSelectAllletters.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSelectAllletters.BackColor = System.Drawing.Color.Transparent;
            this.btnSelectAllletters.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnSelectAllletters.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSelectAllletters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectAllletters.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnSelectAllletters.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnSelectAllletters.Location = new System.Drawing.Point(1, 598);
            this.btnSelectAllletters.Name = "btnSelectAllletters";
            this.btnSelectAllletters.Size = new System.Drawing.Size(110, 28);
            this.btnSelectAllletters.TabIndex = 13;
            this.btnSelectAllletters.TabStop = false;
            this.btnSelectAllletters.Text = "Allow All";
            this.btnSelectAllletters.UseVisualStyleBackColor = false;
            this.btnSelectAllletters.Click += new System.EventHandler(this.btnSelectAllletters_Click);
            // 
            // btnDisallowAllLetters
            // 
            this.btnDisallowAllLetters.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDisallowAllLetters.BackColor = System.Drawing.Color.Transparent;
            this.btnDisallowAllLetters.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnDisallowAllLetters.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDisallowAllLetters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisallowAllLetters.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnDisallowAllLetters.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnDisallowAllLetters.Location = new System.Drawing.Point(113, 598);
            this.btnDisallowAllLetters.Name = "btnDisallowAllLetters";
            this.btnDisallowAllLetters.Size = new System.Drawing.Size(110, 28);
            this.btnDisallowAllLetters.TabIndex = 13;
            this.btnDisallowAllLetters.TabStop = false;
            this.btnDisallowAllLetters.Text = "Disallow All";
            this.btnDisallowAllLetters.UseVisualStyleBackColor = false;
            this.btnDisallowAllLetters.Click += new System.EventHandler(this.btnClearLetters_Click);
            // 
            // cbMapikHei
            // 
            this.cbMapikHei.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMapikHei.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbMapikHei.BackColor = System.Drawing.Color.Lavender;
            this.cbMapikHei.Checked = true;
            this.cbMapikHei.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbMapikHei.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMapikHei.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbMapikHei.Location = new System.Drawing.Point(173, 300);
            this.cbMapikHei.Margin = new System.Windows.Forms.Padding(2);
            this.cbMapikHei.Name = "cbMapikHei";
            this.cbMapikHei.Size = new System.Drawing.Size(40, 41);
            this.cbMapikHei.TabIndex = 6;
            this.cbMapikHei.Text = "הּ";
            this.cbMapikHei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbMapikHei.ThreeState = true;
            this.cbMapikHei.UseVisualStyleBackColor = false;
            this.cbMapikHei.CheckStateChanged += new System.EventHandler(this.cb_CheckedChanged);
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.Color.GhostWhite;
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearch.Controls.Add(this.pnlOptions);
            this.pnlSearch.Controls.Add(this.pnlSpecialEndings);
            this.pnlSearch.Controls.Add(this.pnlLetters);
            this.pnlSearch.Controls.Add(this.pnlNekudos);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearch.Location = new System.Drawing.Point(2, 2);
            this.pnlSearch.Margin = new System.Windows.Forms.Padding(2);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(663, 711);
            this.pnlSearch.TabIndex = 47;
            // 
            // pnlSpecialEndings
            // 
            this.pnlSpecialEndings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSpecialEndings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSpecialEndings.Controls.Add(this.btnSelectAllEndings);
            this.pnlSpecialEndings.Controls.Add(this.btnDisallowAllEndings);
            this.pnlSpecialEndings.Controls.Add(this.cbEndingKumatzYud);
            this.pnlSpecialEndings.Controls.Add(this.cbEndingPasachYud);
            this.pnlSpecialEndings.Controls.Add(this.cbEndingMelupumYud);
            this.pnlSpecialEndings.Controls.Add(this.cbEndingKumatzYudVuv);
            this.pnlSpecialEndings.Controls.Add(this.cbEndingChesPasach);
            this.pnlSpecialEndings.Controls.Add(this.cbEndingHeiPasach);
            this.pnlSpecialEndings.Controls.Add(this.cbEndingKumatzMapikHei);
            this.pnlSpecialEndings.Location = new System.Drawing.Point(-5, -6);
            this.pnlSpecialEndings.Margin = new System.Windows.Forms.Padding(2);
            this.pnlSpecialEndings.Name = "pnlSpecialEndings";
            this.pnlSpecialEndings.Size = new System.Drawing.Size(203, 629);
            this.pnlSpecialEndings.TabIndex = 2;
            // 
            // btnSelectAllEndings
            // 
            this.btnSelectAllEndings.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSelectAllEndings.BackColor = System.Drawing.Color.Transparent;
            this.btnSelectAllEndings.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnSelectAllEndings.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSelectAllEndings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectAllEndings.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnSelectAllEndings.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnSelectAllEndings.Location = new System.Drawing.Point(12, 598);
            this.btnSelectAllEndings.Name = "btnSelectAllEndings";
            this.btnSelectAllEndings.Size = new System.Drawing.Size(75, 28);
            this.btnSelectAllEndings.TabIndex = 14;
            this.btnSelectAllEndings.TabStop = false;
            this.btnSelectAllEndings.Text = "Allow All";
            this.btnSelectAllEndings.UseVisualStyleBackColor = false;
            this.btnSelectAllEndings.Click += new System.EventHandler(this.btnSelectAllEndings_Click);
            // 
            // btnDisallowAllEndings
            // 
            this.btnDisallowAllEndings.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDisallowAllEndings.BackColor = System.Drawing.Color.Transparent;
            this.btnDisallowAllEndings.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnDisallowAllEndings.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDisallowAllEndings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisallowAllEndings.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnDisallowAllEndings.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnDisallowAllEndings.Location = new System.Drawing.Point(102, 598);
            this.btnDisallowAllEndings.Name = "btnDisallowAllEndings";
            this.btnDisallowAllEndings.Size = new System.Drawing.Size(94, 28);
            this.btnDisallowAllEndings.TabIndex = 15;
            this.btnDisallowAllEndings.TabStop = false;
            this.btnDisallowAllEndings.Text = "Disallow All";
            this.btnDisallowAllEndings.UseVisualStyleBackColor = false;
            this.btnDisallowAllEndings.Click += new System.EventHandler(this.btnDisallowAllEndings_Click);
            // 
            // cbEndingKumatzYud
            // 
            this.cbEndingKumatzYud.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndingKumatzYud.BackColor = System.Drawing.Color.Lavender;
            this.cbEndingKumatzYud.Checked = true;
            this.cbEndingKumatzYud.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndingKumatzYud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndingKumatzYud.Font = new System.Drawing.Font("Times New Roman", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndingKumatzYud.Location = new System.Drawing.Point(23, 15);
            this.cbEndingKumatzYud.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndingKumatzYud.Name = "cbEndingKumatzYud";
            this.cbEndingKumatzYud.Size = new System.Drawing.Size(154, 75);
            this.cbEndingKumatzYud.TabIndex = 0;
            this.cbEndingKumatzYud.Text = " ָי";
            this.cbEndingKumatzYud.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndingKumatzYud.ThreeState = true;
            this.cbEndingKumatzYud.UseVisualStyleBackColor = false;
            this.cbEndingKumatzYud.CheckStateChanged += new System.EventHandler(this.cbEnding_CheckedChanged);
            // 
            // cbEndingPasachYud
            // 
            this.cbEndingPasachYud.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndingPasachYud.BackColor = System.Drawing.Color.Lavender;
            this.cbEndingPasachYud.Checked = true;
            this.cbEndingPasachYud.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndingPasachYud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndingPasachYud.Font = new System.Drawing.Font("Times New Roman", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndingPasachYud.Location = new System.Drawing.Point(23, 95);
            this.cbEndingPasachYud.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndingPasachYud.Name = "cbEndingPasachYud";
            this.cbEndingPasachYud.Size = new System.Drawing.Size(154, 75);
            this.cbEndingPasachYud.TabIndex = 1;
            this.cbEndingPasachYud.Text = " ַי";
            this.cbEndingPasachYud.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndingPasachYud.ThreeState = true;
            this.cbEndingPasachYud.UseVisualStyleBackColor = false;
            this.cbEndingPasachYud.CheckStateChanged += new System.EventHandler(this.cbEnding_CheckedChanged);
            // 
            // cbEndingMelupumYud
            // 
            this.cbEndingMelupumYud.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndingMelupumYud.BackColor = System.Drawing.Color.Lavender;
            this.cbEndingMelupumYud.Checked = true;
            this.cbEndingMelupumYud.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndingMelupumYud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndingMelupumYud.Font = new System.Drawing.Font("Times New Roman", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndingMelupumYud.Location = new System.Drawing.Point(23, 175);
            this.cbEndingMelupumYud.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndingMelupumYud.Name = "cbEndingMelupumYud";
            this.cbEndingMelupumYud.Size = new System.Drawing.Size(154, 75);
            this.cbEndingMelupumYud.TabIndex = 2;
            this.cbEndingMelupumYud.Text = "וּי";
            this.cbEndingMelupumYud.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndingMelupumYud.ThreeState = true;
            this.cbEndingMelupumYud.UseVisualStyleBackColor = false;
            this.cbEndingMelupumYud.CheckStateChanged += new System.EventHandler(this.cbEnding_CheckedChanged);
            // 
            // cbEndingKumatzYudVuv
            // 
            this.cbEndingKumatzYudVuv.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndingKumatzYudVuv.BackColor = System.Drawing.Color.Lavender;
            this.cbEndingKumatzYudVuv.Checked = true;
            this.cbEndingKumatzYudVuv.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndingKumatzYudVuv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndingKumatzYudVuv.Font = new System.Drawing.Font("Times New Roman", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndingKumatzYudVuv.Location = new System.Drawing.Point(23, 255);
            this.cbEndingKumatzYudVuv.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndingKumatzYudVuv.Name = "cbEndingKumatzYudVuv";
            this.cbEndingKumatzYudVuv.Size = new System.Drawing.Size(154, 75);
            this.cbEndingKumatzYudVuv.TabIndex = 3;
            this.cbEndingKumatzYudVuv.Text = " ָיו";
            this.cbEndingKumatzYudVuv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndingKumatzYudVuv.ThreeState = true;
            this.cbEndingKumatzYudVuv.UseVisualStyleBackColor = false;
            this.cbEndingKumatzYudVuv.CheckStateChanged += new System.EventHandler(this.cbEnding_CheckedChanged);
            // 
            // cbEndingChesPasach
            // 
            this.cbEndingChesPasach.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndingChesPasach.BackColor = System.Drawing.Color.Lavender;
            this.cbEndingChesPasach.Checked = true;
            this.cbEndingChesPasach.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndingChesPasach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndingChesPasach.Font = new System.Drawing.Font("Times New Roman", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndingChesPasach.Location = new System.Drawing.Point(23, 335);
            this.cbEndingChesPasach.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndingChesPasach.Name = "cbEndingChesPasach";
            this.cbEndingChesPasach.Size = new System.Drawing.Size(154, 75);
            this.cbEndingChesPasach.TabIndex = 4;
            this.cbEndingChesPasach.Text = "חַ";
            this.cbEndingChesPasach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndingChesPasach.ThreeState = true;
            this.cbEndingChesPasach.UseVisualStyleBackColor = false;
            this.cbEndingChesPasach.CheckStateChanged += new System.EventHandler(this.cbEnding_CheckedChanged);
            // 
            // cbEndingHeiPasach
            // 
            this.cbEndingHeiPasach.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndingHeiPasach.BackColor = System.Drawing.Color.Lavender;
            this.cbEndingHeiPasach.Checked = true;
            this.cbEndingHeiPasach.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndingHeiPasach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndingHeiPasach.Font = new System.Drawing.Font("Times New Roman", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndingHeiPasach.Location = new System.Drawing.Point(23, 415);
            this.cbEndingHeiPasach.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndingHeiPasach.Name = "cbEndingHeiPasach";
            this.cbEndingHeiPasach.Size = new System.Drawing.Size(154, 75);
            this.cbEndingHeiPasach.TabIndex = 5;
            this.cbEndingHeiPasach.Text = "הַ";
            this.cbEndingHeiPasach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndingHeiPasach.ThreeState = true;
            this.cbEndingHeiPasach.UseVisualStyleBackColor = false;
            this.cbEndingHeiPasach.CheckStateChanged += new System.EventHandler(this.cbEnding_CheckedChanged);
            // 
            // cbEndingKumatzMapikHei
            // 
            this.cbEndingKumatzMapikHei.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEndingKumatzMapikHei.BackColor = System.Drawing.Color.Lavender;
            this.cbEndingKumatzMapikHei.Checked = true;
            this.cbEndingKumatzMapikHei.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEndingKumatzMapikHei.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEndingKumatzMapikHei.Font = new System.Drawing.Font("Times New Roman", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cbEndingKumatzMapikHei.Location = new System.Drawing.Point(23, 495);
            this.cbEndingKumatzMapikHei.Margin = new System.Windows.Forms.Padding(2);
            this.cbEndingKumatzMapikHei.Name = "cbEndingKumatzMapikHei";
            this.cbEndingKumatzMapikHei.Size = new System.Drawing.Size(154, 75);
            this.cbEndingKumatzMapikHei.TabIndex = 6;
            this.cbEndingKumatzMapikHei.Text = " ָהּ\r\n";
            this.cbEndingKumatzMapikHei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEndingKumatzMapikHei.ThreeState = true;
            this.cbEndingKumatzMapikHei.UseVisualStyleBackColor = false;
            this.cbEndingKumatzMapikHei.CheckStateChanged += new System.EventHandler(this.cbEnding_CheckedChanged);
            // 
            // btnToggleSearch
            // 
            this.btnToggleSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToggleSearch.BackColor = System.Drawing.SystemColors.Control;
            this.btnToggleSearch.BackgroundImage = global::KriahTech.Properties.Resources.SearchHide;
            this.btnToggleSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnToggleSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnToggleSearch.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnToggleSearch.FlatAppearance.BorderSize = 0;
            this.btnToggleSearch.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.btnToggleSearch.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.btnToggleSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToggleSearch.Location = new System.Drawing.Point(392, 640);
            this.btnToggleSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnToggleSearch.Name = "btnToggleSearch";
            this.btnToggleSearch.Size = new System.Drawing.Size(45, 80);
            this.btnToggleSearch.TabIndex = 2;
            this.btnToggleSearch.TabStop = false;
            this.btnToggleSearch.UseVisualStyleBackColor = false;
            this.btnToggleSearch.Click += new System.EventHandler(this.btnToggleSearch_Click);
            // 
            // pnlResults
            // 
            this.pnlResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlResults.Controls.Add(this.webBrowser1);
            this.pnlResults.Location = new System.Drawing.Point(1, 0);
            this.pnlResults.Margin = new System.Windows.Forms.Padding(2);
            this.pnlResults.Name = "pnlResults";
            this.pnlResults.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.pnlResults.Size = new System.Drawing.Size(417, 724);
            this.pnlResults.TabIndex = 48;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.Margin = new System.Windows.Forms.Padding(2);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(17, 16);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(417, 724);
            this.webBrowser1.TabIndex = 1;
            // 
            // pnlAdvancedRules
            // 
            this.pnlAdvancedRules.BackColor = System.Drawing.Color.Lavender;
            this.pnlAdvancedRules.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAdvancedRules.Controls.Add(this.label5);
            this.pnlAdvancedRules.Controls.Add(this.label4);
            this.pnlAdvancedRules.Controls.Add(this.btnGoAdvncFilter);
            this.pnlAdvancedRules.Controls.Add(this.radioButton11);
            this.pnlAdvancedRules.Controls.Add(this.radioButton10);
            this.pnlAdvancedRules.Controls.Add(this.radioButton9);
            this.pnlAdvancedRules.Controls.Add(this.radioButton8);
            this.pnlAdvancedRules.Controls.Add(this.btnGoAdvancedRules);
            this.pnlAdvancedRules.Controls.Add(this.label3);
            this.pnlAdvancedRules.Controls.Add(this.radioButton6);
            this.pnlAdvancedRules.Controls.Add(this.radioButton5);
            this.pnlAdvancedRules.Controls.Add(this.radioButton7);
            this.pnlAdvancedRules.Controls.Add(this.radioButton4);
            this.pnlAdvancedRules.Controls.Add(this.radioButton3);
            this.pnlAdvancedRules.Controls.Add(this.radioButton2);
            this.pnlAdvancedRules.Controls.Add(this.radioButton1);
            this.pnlAdvancedRules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAdvancedRules.Location = new System.Drawing.Point(2, 2);
            this.pnlAdvancedRules.Margin = new System.Windows.Forms.Padding(2);
            this.pnlAdvancedRules.Name = "pnlAdvancedRules";
            this.pnlAdvancedRules.Size = new System.Drawing.Size(663, 711);
            this.pnlAdvancedRules.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(298, 655);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(360, 42);
            this.label5.TabIndex = 15;
            this.label5.Text = "Search for words that match the the Letters and Nekudos \r\ncurrently selected in t" +
    "he Regular Search Tab and also \r\nmatch the rule selected above";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 655);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(261, 28);
            this.label4.TabIndex = 14;
            this.label4.Text = "Search all words in the chosen list for the \r\nones that match the rule selected a" +
    "bove";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnGoAdvncFilter
            // 
            this.btnGoAdvncFilter.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGoAdvncFilter.Font = new System.Drawing.Font("Arial Black", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnGoAdvncFilter.ForeColor = System.Drawing.Color.Maroon;
            this.btnGoAdvncFilter.Location = new System.Drawing.Point(369, 617);
            this.btnGoAdvncFilter.Margin = new System.Windows.Forms.Padding(2);
            this.btnGoAdvncFilter.Name = "btnGoAdvncFilter";
            this.btnGoAdvncFilter.Size = new System.Drawing.Size(218, 36);
            this.btnGoAdvncFilter.TabIndex = 13;
            this.btnGoAdvncFilter.Text = "Add To Filter";
            this.btnGoAdvncFilter.UseVisualStyleBackColor = true;
            this.btnGoAdvncFilter.Click += new System.EventHandler(this.btnGoAdvncFilter_Click);
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton11.Location = new System.Drawing.Point(37, 564);
            this.radioButton11.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(545, 33);
            this.radioButton11.TabIndex = 12;
            this.radioButton11.Tag = "^.+י(?=[{LETTERS}]).+$";
            this.radioButton11.Text = "Has a Yud in middle of word without a Nekudah.";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton10.Location = new System.Drawing.Point(37, 515);
            this.radioButton10.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(542, 33);
            this.radioButton10.TabIndex = 11;
            this.radioButton10.Tag = "^.+ו(?=[{LETTERS}]).+$";
            this.radioButton10.Text = "Has a Vov in middle of word without a Nekudah.";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton9.Location = new System.Drawing.Point(37, 466);
            this.radioButton9.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(556, 33);
            this.radioButton9.TabIndex = 10;
            this.radioButton9.Tag = "^.+א(?=[{LETTERS}]).+$";
            this.radioButton9.Text = "Has an Alef in middle of word without a Nekudah.";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton8.Location = new System.Drawing.Point(37, 417);
            this.radioButton8.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(621, 33);
            this.radioButton8.TabIndex = 9;
            this.radioButton8.Tag = "^.([{LETTERS}](?<![אוי])|[{NEKUDAS}])+.$";
            this.radioButton8.Text = "No Alef, Vov, Yud in middle of word without a Nekudah.";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // btnGoAdvancedRules
            // 
            this.btnGoAdvancedRules.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnGoAdvancedRules.Font = new System.Drawing.Font("Arial Black", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnGoAdvancedRules.ForeColor = System.Drawing.Color.Maroon;
            this.btnGoAdvancedRules.Location = new System.Drawing.Point(29, 617);
            this.btnGoAdvancedRules.Margin = new System.Windows.Forms.Padding(2);
            this.btnGoAdvancedRules.Name = "btnGoAdvancedRules";
            this.btnGoAdvancedRules.Size = new System.Drawing.Size(218, 36);
            this.btnGoAdvancedRules.TabIndex = 7;
            this.btnGoAdvancedRules.Text = "Search All";
            this.btnGoAdvancedRules.UseVisualStyleBackColor = true;
            this.btnGoAdvancedRules.Click += new System.EventHandler(this.btnGoAdvancedRules_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(228, 631);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(204, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Loading word list.... ";
            this.label3.Visible = false;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton6.Location = new System.Drawing.Point(37, 65);
            this.radioButton6.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(412, 33);
            this.radioButton6.TabIndex = 5;
            this.radioButton6.Tag = "^[^ְ]{2}.*ְ+.*[^ְ]{2}$";
            this.radioButton6.Text = "Shva only in the middle of the word ";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton5.Location = new System.Drawing.Point(37, 114);
            this.radioButton5.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(323, 33);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.Tag = "^.+ְ$";
            this.radioButton5.Text = "Shva at the end of the word";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton7.Location = new System.Drawing.Point(37, 212);
            this.radioButton7.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(389, 33);
            this.radioButton7.TabIndex = 3;
            this.radioButton7.Tag = "ְ.ְ$";
            this.radioButton7.Text = "Two Shvas at the end of the word";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton4.Location = new System.Drawing.Point(37, 163);
            this.radioButton4.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(395, 33);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.Tag = "ְ.ְ";
            this.radioButton4.Text = "Two Shvas adjacent to each other";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton3.Location = new System.Drawing.Point(37, 339);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(498, 62);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Tag = "(.)ְ\\1";
            this.radioButton3.Text = "Two identical letters adjacent to each other,\r\nwhere the first is a Shva.";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton2.Location = new System.Drawing.Point(37, 261);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(498, 62);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Tag = "(.)[ְֱֲֳִֵֶַָֹֻ]?\\1ְ";
            this.radioButton2.Text = "Two identical letters adjacent to each other,\r\nwhere the second is a Shva.";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.radioButton1.Location = new System.Drawing.Point(37, 16);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(369, 33);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "^.ְ";
            this.radioButton1.Text = "Shva in the beginning of a word";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // tabControlSearch
            // 
            this.tabControlSearch.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControlSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlSearch.Controls.Add(this.tabPage1);
            this.tabControlSearch.Controls.Add(this.tabPage2);
            this.tabControlSearch.HotTrack = true;
            this.tabControlSearch.Location = new System.Drawing.Point(415, 0);
            this.tabControlSearch.Margin = new System.Windows.Forms.Padding(2);
            this.tabControlSearch.Multiline = true;
            this.tabControlSearch.Name = "tabControlSearch";
            this.tabControlSearch.SelectedIndex = 0;
            this.tabControlSearch.Size = new System.Drawing.Size(699, 723);
            this.tabControlSearch.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pnlSearch);
            this.tabPage1.Location = new System.Drawing.Point(28, 4);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(667, 715);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Regular Search";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pnlAdvancedRules);
            this.tabPage2.Location = new System.Drawing.Point(28, 4);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(667, 715);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Advanced Search";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(240, 56);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(239, 26);
            this.toolStripMenuItem1.Text = "Get &Serial Code";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(239, 26);
            this.toolStripMenuItem2.Text = "Test &Regular Expression";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // cbIncludeAdvancedFilter
            // 
            this.cbIncludeAdvancedFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbIncludeAdvancedFilter.AutoSize = true;
            this.cbIncludeAdvancedFilter.Location = new System.Drawing.Point(10, 66);
            this.cbIncludeAdvancedFilter.Name = "cbIncludeAdvancedFilter";
            this.cbIncludeAdvancedFilter.Size = new System.Drawing.Size(165, 19);
            this.cbIncludeAdvancedFilter.TabIndex = 18;
            this.cbIncludeAdvancedFilter.Text = "Include advanced search";
            this.cbIncludeAdvancedFilter.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1114, 723);
            this.Controls.Add(this.btnToggleSearch);
            this.Controls.Add(this.tabControlSearch);
            this.Controls.Add(this.pnlResults);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Kriah Tech";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlOptions.ResumeLayout(false);
            this.pnlOptions.PerformLayout();
            this.pnlNekudos.ResumeLayout(false);
            this.pnlLetters.ResumeLayout(false);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSpecialEndings.ResumeLayout(false);
            this.pnlResults.ResumeLayout(false);
            this.pnlAdvancedRules.ResumeLayout(false);
            this.pnlAdvancedRules.PerformLayout();
            this.tabControlSearch.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.CheckBox cbAlef;
        private System.Windows.Forms.CheckBox cbBeis;
        private System.Windows.Forms.CheckBox cbVeis;
        private System.Windows.Forms.CheckBox cbGimmel;
        private System.Windows.Forms.CheckBox cbDaled;
        private System.Windows.Forms.CheckBox cbHei;
        private System.Windows.Forms.CheckBox cbVav;
        private System.Windows.Forms.CheckBox cbZayin;
        private System.Windows.Forms.CheckBox cbChes;
        private System.Windows.Forms.CheckBox cbTes;
        private System.Windows.Forms.CheckBox cbYud;
        private System.Windows.Forms.CheckBox cbKof;
        private System.Windows.Forms.CheckBox cbChof;
        private System.Windows.Forms.CheckBox cbEndKof;
        private System.Windows.Forms.CheckBox cbEndChof;
        private System.Windows.Forms.CheckBox cbLamed;
        private System.Windows.Forms.CheckBox cbMem;
        private System.Windows.Forms.CheckBox cbEndMem;
        private System.Windows.Forms.CheckBox cbNun;
        private System.Windows.Forms.CheckBox cbEndNun;
        private System.Windows.Forms.CheckBox cbSamech;
        private System.Windows.Forms.CheckBox cbAyin;
        private System.Windows.Forms.CheckBox cbPay;
        private System.Windows.Forms.CheckBox cbFay;
        private System.Windows.Forms.CheckBox cbEndFay;
        private System.Windows.Forms.CheckBox cbTzadik;
        private System.Windows.Forms.CheckBox cbEndTzadik;
        private System.Windows.Forms.CheckBox cbKuf;
        private System.Windows.Forms.CheckBox cbReish;
        private System.Windows.Forms.CheckBox cbShin;
        private System.Windows.Forms.CheckBox cbSin;
        private System.Windows.Forms.CheckBox cbTuf;
        private System.Windows.Forms.CheckBox cbSuf;
        private System.Windows.Forms.CheckBox cbKumatz;
        private System.Windows.Forms.CheckBox cbPasach;
        private System.Windows.Forms.CheckBox cbChirik;
        private System.Windows.Forms.CheckBox cbSegol;
        private System.Windows.Forms.CheckBox cbTzeirei;
        private System.Windows.Forms.CheckBox cbShva;
        private System.Windows.Forms.CheckBox cbCholam;
        private System.Windows.Forms.CheckBox cbCholumVav;
        private System.Windows.Forms.CheckBox cbKubutz;
        private System.Windows.Forms.CheckBox cbMelupumBegining;
        private System.Windows.Forms.CheckBox cbChatafSegol;
        private System.Windows.Forms.CheckBox cbChatafKumatz;
        private System.Windows.Forms.CheckBox cbChatafPasach;
        private System.Windows.Forms.Panel pnlOptions;
        private System.Windows.Forms.Panel pnlLetters;
        private System.Windows.Forms.Panel pnlNekudos;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Button btnToggleSearch;
        private System.Windows.Forms.Button btnDisallowAllNekudos;
        private System.Windows.Forms.Panel pnlResults;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbMapikHei;
        private System.Windows.Forms.Panel pnlSpecialEndings;
        private System.Windows.Forms.CheckBox cbEndingKumatzYud;
        private System.Windows.Forms.CheckBox cbEndingPasachYud;
        private System.Windows.Forms.CheckBox cbEndingMelupumYud;
        private System.Windows.Forms.CheckBox cbEndingKumatzYudVuv;
        private System.Windows.Forms.CheckBox cbEndingChesPasach;
        private System.Windows.Forms.CheckBox cbEndingHeiPasach;
        private System.Windows.Forms.CheckBox cbEndingKumatzMapikHei;
        private System.Windows.Forms.Panel pnlAdvancedRules;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button btnGoAdvancedRules;
        private System.Windows.Forms.TabControl tabControlSearch;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.Button btnSelectAllNekudos;
        private System.Windows.Forms.Button btnSelectAllletters;
        private System.Windows.Forms.Button btnDisallowAllLetters;
        private System.Windows.Forms.LinkLabel llAdmin;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.CheckBox cbMelupumMiddle;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.Button btnSelectAllEndings;
        private System.Windows.Forms.Button btnDisallowAllEndings;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnGoAdvncFilter;
        private System.Windows.Forms.CheckBox cbIncludeAdvancedFilter;
    }
}

