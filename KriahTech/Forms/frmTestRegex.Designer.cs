﻿namespace KriahTech
{
    partial class frmTestRegex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbInfo = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlPHValues = new System.Windows.Forms.Panel();
            this.llToggleResults = new System.Windows.Forms.LinkLabel();
            this.lblResults = new System.Windows.Forms.Label();
            this.txtPHValues = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.pnlPHValues.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button1.Location = new System.Drawing.Point(785, 444);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 56);
            this.button1.TabIndex = 0;
            this.button1.Text = "Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.textBox1.ForeColor = System.Drawing.Color.SlateGray;
            this.textBox1.Location = new System.Drawing.Point(10, 378);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(938, 45);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 100F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(15, 436);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(735, 204);
            this.label1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox2.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.textBox2.ForeColor = System.Drawing.Color.Maroon;
            this.textBox2.Location = new System.Drawing.Point(10, 31);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(938, 304);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "^{0}?([{1}][{2}](([{1}][{2}])|[{3}])*[{4}][{5}]?$)|([{1}][{6}]$)";
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Pattern:";
            // 
            // lbInfo
            // 
            this.lbInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfo.BackColor = System.Drawing.Color.White;
            this.lbInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbInfo.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbInfo.ForeColor = System.Drawing.Color.Black;
            this.lbInfo.FormattingEnabled = true;
            this.lbInfo.ItemHeight = 57;
            this.lbInfo.Location = new System.Drawing.Point(998, 45);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbInfo.Size = new System.Drawing.Size(417, 570);
            this.lbInfo.TabIndex = 6;
            this.lbInfo.Visible = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 356);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Text to test:";
            // 
            // pnlPHValues
            // 
            this.pnlPHValues.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPHValues.BackColor = System.Drawing.Color.Lavender;
            this.pnlPHValues.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPHValues.Controls.Add(this.llToggleResults);
            this.pnlPHValues.Controls.Add(this.lblResults);
            this.pnlPHValues.Controls.Add(this.txtPHValues);
            this.pnlPHValues.Location = new System.Drawing.Point(974, 5);
            this.pnlPHValues.Name = "pnlPHValues";
            this.pnlPHValues.Size = new System.Drawing.Size(466, 627);
            this.pnlPHValues.TabIndex = 7;
            // 
            // llToggleResults
            // 
            this.llToggleResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llToggleResults.BackColor = System.Drawing.Color.Transparent;
            this.llToggleResults.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.llToggleResults.Location = new System.Drawing.Point(320, 3);
            this.llToggleResults.Name = "llToggleResults";
            this.llToggleResults.Size = new System.Drawing.Size(141, 13);
            this.llToggleResults.TabIndex = 7;
            this.llToggleResults.TabStop = true;
            this.llToggleResults.Text = "Show Results";
            this.llToggleResults.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.llToggleResults.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llToggleResults_LinkClicked);
            // 
            // lblResults
            // 
            this.lblResults.AutoSize = true;
            this.lblResults.Location = new System.Drawing.Point(16, 12);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(427, 16);
            this.lblResults.TabIndex = 6;
            this.lblResults.Text = "Available placeholders: (use numbers surrounded by curly braces)";
            // 
            // txtPHValues
            // 
            this.txtPHValues.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPHValues.BackColor = System.Drawing.Color.White;
            this.txtPHValues.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtPHValues.Location = new System.Drawing.Point(23, 37);
            this.txtPHValues.Multiline = true;
            this.txtPHValues.Name = "txtPHValues";
            this.txtPHValues.ReadOnly = true;
            this.txtPHValues.Size = new System.Drawing.Size(417, 571);
            this.txtPHValues.TabIndex = 0;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(913, 338);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(45, 16);
            this.linkLabel1.TabIndex = 8;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Reset";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // frmTestRegex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(1450, 657);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pnlPHValues);
            this.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Name = "frmTestRegex";
            this.ShowIcon = false;
            this.Text = "Test Regular Expression";
            this.pnlPHValues.ResumeLayout(false);
            this.pnlPHValues.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lbInfo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlPHValues;
        private System.Windows.Forms.Label lblResults;
        private System.Windows.Forms.TextBox txtPHValues;
        private System.Windows.Forms.LinkLabel llToggleResults;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}