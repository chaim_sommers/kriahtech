﻿using System;
using System.Windows.Forms;

namespace KriahTech
{
    public partial class frmEnterSerialCode : Form
    {
        public frmEnterSerialCode()
        {
            InitializeComponent();
        }

        private void frmEnterSerialCode_Load(object sender, EventArgs e)
        {
            string random = Oshek.OshekManager.UnMuxFromStorage(Properties.Settings.Default.OshekHiR);
            this.lblInitKey.Text = Oshek.OshekManager.GetInitiationKey(random) + 'Y' + random;
        }

        private void frmEnterSerialCode_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] initKeyParts = this.lblInitKey.Text.Split('Y');
            if (this.textBox1.Text == Oshek.OshekManager.GetSerialCode(initKeyParts[0], initKeyParts[1]))
            {
                Properties.Settings.Default.OshekLoS = Oshek.OshekManager.GetMuxedForStorage(this.textBox1.Text);
                Properties.Settings.Default.Save();
                var frm1 = new Form1();
                //we are the main form right now and we want that when form1 is closed the application exits.
                frm1.FormClosed += delegate { this.Close(); };
                frm1.Show();
                this.Hide();
            }
            else
            {
                this.lblError.Visible = true;
            }
        }       

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.lblError.Visible = false;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Clipboard.SetText(this.lblInitKey.Text);
        }        
    }
}
