﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace KriahTech
{
    public partial class Form1 : Form
    {
        private bool _isSearchPanelOpen = true;
        public static ClientController ClientController = new ClientController();

        public Form1()
        {
            InitializeComponent();
            this.webBrowser1.ObjectForScripting = ClientController;
            Properties.Settings.Default.PropertyChanged += Default_PropertyChanged;
            ClientController.CurrentHtmlChanged += delegate (string html)
            {
                this.webBrowser1.DocumentText = html;
            };
            this.llAdmin.Visible = Program.IsAdminRun;
        }

        #region event handlers
        private void Form1_Load(object sender, EventArgs e)
        {
            this.ShowStartupMessage();
            this.InitText();
            this.InitNekudaTags();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            this.DoRegularSearch();
        }

        private void btnToggleSearch_Click(object sender, EventArgs e)
        {
            this.ToggleSearchPanels();
        }

        private void cbKumatz_CheckStateChanged(object sender, EventArgs e)
        {
            cb_CheckedChanged(sender, e);
            if (cbKumatz.CheckState != CheckState.Unchecked && Properties.Settings.Default.IncludeChatafs && !cbChatafKumatz.Checked)
            {
                cbChatafKumatz.Checked = true;
            }
        }

        private void cbPasach_CheckStateChanged(object sender, EventArgs e)
        {
            cb_CheckedChanged(sender, e);
            if (cbPasach.CheckState != CheckState.Unchecked && Properties.Settings.Default.IncludeChatafs && !cbChatafPasach.Checked)
            {
                cbChatafPasach.Checked = true;
            }
        }

        private void cbSegol_CheckStateChanged(object sender, EventArgs e)
        {
            cb_CheckedChanged(sender, e);
            if (cbSegol.CheckState != CheckState.Unchecked && Properties.Settings.Default.IncludeChatafs && !cbChatafSegol.Checked)
            {
                cbChatafSegol.Checked = true;
            }
        }

        private void Default_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IncludeChatafs":
                    if (Properties.Settings.Default.IncludeChatafs)
                    {
                        if (cbKumatz.CheckState != CheckState.Unchecked && cbChatafKumatz.CheckState == CheckState.Unchecked)
                        {
                            cbChatafKumatz.CheckState = CheckState.Checked;
                        }
                        if (cbPasach.CheckState != CheckState.Unchecked && cbChatafPasach.CheckState == CheckState.Unchecked)
                        {
                            cbChatafPasach.CheckState = CheckState.Checked;
                        }
                        if (cbSegol.CheckState != CheckState.Unchecked && cbChatafSegol.CheckState == CheckState.Unchecked)
                        {
                            cbChatafSegol.CheckState = CheckState.Checked;
                        }
                    }
                    break;
                case "CurrentFileName":
                    this.InitText();
                    break;
                case "NumberOfWordsPerGroup":
                case "NumberOfColumns":
                case "FontSize":
                case "FontFamily":
                case "ShowResultsInColor":
                case "HideChatafs":
                case "IgnoredWords":
                    ClientController.RefreshHtml();
                    break;
            }
        }

        /// <summary>
        /// For letters and nekudos: Checked: is allowed, Indeterminate: required, UnChecked: not allowed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            //CheckState.Checked means the results are allowed to contain this character or ending
            if (cb.CheckState == CheckState.Checked)
            {
                cb.BackColor = Color.Lavender;
            }
            //CheckState.Indeterminate means the results must contain this character or ending
            else if (cb.CheckState == CheckState.Indeterminate)
            {
                cb.BackColor = Color.LightGreen;
            }
            //CheckState.UnChecked means the results are not allowed to contain this character or ending
            else
            {
                cb.BackColor = Color.LightSalmon;
            }
        }

        /// <summary>
        /// For endings: Checked: is allowed, Indeterminate: not allowed, UnChecked: required.        
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbEnding_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            //CheckState.Checked means the results are allowed to contain this character or ending
            if (cb.CheckState == CheckState.Checked)
            {
                cb.BackColor = Color.Lavender;
            }

            // CheckState.Indeterminate means the results can not contain this character or ending.

            // The reason why it was done this way, rather than have UnChecked mean "not allowed", is because:
            // we only allow a single required ending, and we automatically 
            // check all allow all others if one of them is set to "required".
            // The Check-boxes start out by default as Checked (allowed).
            // So, by clicking once, it turns it to Indeterminate and twice to unchecked.
            // Therefore, if Indeterminate meant required, it would be impossible to have multiple 
            // not-allowed endings, as the first one would get set back to Checked when the 
            // second one is Clicked once and set to Indeterminate. (get it?)
            // We could of had the default be UnChecked and have that mean allowed, 
            // but as all the letters and Nekudos start out Checked,
            // We wanted them all to look the same when the program starts up.

            else if (cb.CheckState == CheckState.Indeterminate)
            {
                cb.BackColor = Color.LightSalmon;
            }
            //CheckState.UnChecked means the results must contain this character or ending
            else
            {
                cb.BackColor = Color.LightGreen;
                //There can only be one required ending.
                foreach (var c in this.pnlSpecialEndings.Controls.OfType<CheckBox>().Where(b =>
                    b != cb && b.CheckState == CheckState.Unchecked))
                {
                    c.CheckState = CheckState.Checked;
                }
            }
        }

        private void btnClearLetters_Click(object sender, EventArgs e)
        {
            foreach (var cb in this.pnlLetters.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Unchecked))
            {
                cb.CheckState = CheckState.Unchecked;
            }
        }

        private void btnClearNekudos_Click(object sender, EventArgs e)
        {
            foreach (var cb in this.pnlNekudos.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Unchecked))
            {
                cb.CheckState = CheckState.Unchecked;
            }
        }

        private void btnSelectAllNekudos_Click(object sender, EventArgs e)
        {
            foreach (var cb in this.pnlNekudos.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Checked))
            {
                cb.CheckState = CheckState.Checked;
            }
        }

        private void btnSelectAllletters_Click(object sender, EventArgs e)
        {
            foreach (var cb in this.pnlLetters.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Checked))
            {
                cb.CheckState = CheckState.Checked;
            }
        }

        private void btnSelectAllEndings_Click(object sender, EventArgs e)
        {
            foreach (var cb in this.pnlSpecialEndings.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Checked))
            {
                cb.CheckState = CheckState.Checked;
            }
        }

        private void btnDisallowAllEndings_Click(object sender, EventArgs e)
        {
            foreach (var cb in this.pnlSpecialEndings.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Indeterminate))
            {
                cb.CheckState = CheckState.Indeterminate;
            }
        }

        private void btnGoAdvancedRules_Click(object sender, EventArgs e)
        {
            this.DoAdvancedSearch(false);
        }

        private void btnGoAdvncFilter_Click(object sender, EventArgs e)
        {
            this.DoAdvancedSearch(true);
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Program.ShowSettings();
        }

        private void llAdmin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.contextMenuStrip1.Show(this.llAdmin, 0, 0);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmViewSerialCode().Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            new frmTestRegex().Show();
        }
        #endregion

        #region private functions
        private void DoRegularSearch()
        {
            if (this.cbIncludeAdvancedFilter.Checked)
            {
                this.DoAdvancedSearch(true);
            }
            else
            {
                IEnumerable<CheckBox> letters, nekudos, ending;
                string mustHaveChars;
                this.GetItemsForFilter(out letters, out nekudos, out ending, out mustHaveChars);

                if (letters.Count() == 0)
                {
                    ShowErrMessage(@"This program is lazy.<br />
                    It does not feel terribly compelled to search through all words in the list<br/>
                    when any word that has <strong>ANY LETTERS AT ALL</strong> are not allowed...");
                    return;
                }
                if (nekudos.Count() == 0)
                {
                    ShowErrMessage("No Nekudos allowed?<br />Hm... this is a Kriah program not a newspaper!");
                    return;
                }

                this.ToggleSearchButton(false);
                this.SetRegularResults(letters, nekudos, ending);
                this.AfterSearch();
            }
        }

        private void DoAdvancedSearch(bool addToFilter)
        {
            this.ToggleSearchButton(false);
            this.SetResultsAdvancedRules(addToFilter);
            this.AfterSearch();
        }

        private void ToggleSearchButton(bool visible)
        {
            this.Cursor = visible ? Cursors.Default : Cursors.WaitCursor;
            this.label2.Visible = this.label3.Visible = !visible;
            this.btnGoAdvancedRules.Visible =
                this.btnGo.Visible =
                this.btnGoAdvncFilter.Visible =
                this.cbIncludeAdvancedFilter.Visible =
                this.label4.Visible =
                this.label5.Visible = visible;
            this.Refresh();
        }

        /// <summary>
        /// Set the results
        /// </summary>
        /// <param name="letters"></param>
        /// <param name="nekudos"></param>
        /// <param name="ending"></param>
        private void SetRegularResults(IEnumerable<CheckBox> letters, IEnumerable<CheckBox> nekudos, IEnumerable<CheckBox> ending)
        {
            // For letters and nekudos: Checked: is allowed, Indeterminate: required, UnChecked: not allowed
            // For endings: Checked: is allowed, Indeterminate: not allowed, UnChecked: required

            string mustHaveChars = string.Join("", letters
                                    //CheckState.Indeterminate means the word must have this character
                                    .Where(cb => cb.CheckState == CheckState.Indeterminate)
                                    .Select(cb => cb.Text.Trim())) +
                                    string.Join("", nekudos
                                    .Where(cb => cb != this.cbMelupumBegining && cb.CheckState == CheckState.Indeterminate)
                                    .Select(cb => cb.Tag));

            WordsFunctions.FindInCurrentFile(
                BuildRegEx(letters, nekudos, ending),
                mustHaveChars);

            ClientController.CurrentPageNumber = 0;
        }

        private void SetResultsAdvancedRules(bool addToFilter)
        {
            string regexAdv = ((string)this.pnlAdvancedRules.Controls.OfType<RadioButton>().Single(rb =>
                rb.Checked).Tag)
                .Replace("{LETTERS}", new String(WordProcess.Letters))
                .Replace("{NEKUDAS}", new String(WordProcess.Nekudos));
            if (addToFilter)
            {
                IEnumerable<CheckBox> letters, nekudos, ending;
                string mustHaveChars;
                this.GetItemsForFilter(out letters, out nekudos, out ending, out mustHaveChars);
                if (letters.Count() == 0)
                {
                    ShowErrMessage(@"How does one expect to find any words if no letters are allowed?");
                    return;
                }
                if (nekudos.Count() == 0)
                {
                    ShowErrMessage("No Nekudos allowed? It's not going to happen.");
                    return;
                }
                //First, the chosen word list is filtered for those that match the regular search criteria
                WordsFunctions.FindInCurrentFile(
                    BuildRegEx(letters, nekudos, ending),
                    mustHaveChars);
                if (Program.CurrentResults.Count() > 0)
                {
                    //The results from the regular search are filtered 
                    //for those that match the selected advanced search.
                    WordsFunctions.FilterCurrentList(regexAdv);
                }
            }
            else
            {
                WordsFunctions.FindInCurrentFile(regexAdv);
            }
            ClientController.CurrentPageNumber = 0;
            if (Program.CurrentResults.Count() == 0)
            {
                ShowErrMessage("There are no words that match the given criteria.");
            }
            else
            {
                ClientController.RefreshHtml();
            }
        }

        /// <summary>
        /// Sets its arguments to only those checkboxes that will have an affect on the search results.
        /// </summary>
        /// <param name="letters">The allowed and required Letter checkboxes</param>
        /// <param name="nekudos">The allowed and required Nekuda checkboxes</param>
        /// <param name="ending">The not-allowed and required Ending checkboxes</param>
        /// <param name="mustHaveChars">String of characters that are required somewhere in the word</param>
        private void GetItemsForFilter(out IEnumerable<CheckBox> letters, out IEnumerable<CheckBox> nekudos, out IEnumerable<CheckBox> ending, out string mustHaveChars)
        {
            //For letters and nekudos: Checked: is allowed, Indeterminate: required, UnChecked: not allowed 
            letters = this.pnlLetters.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Unchecked);
            nekudos = this.pnlNekudos.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Unchecked);
            //For endings: Checked: is allowed, Indeterminate: not allowed, UnChecked: required
            ending = this.pnlSpecialEndings.Controls.OfType<CheckBox>().Where(cb =>
                cb.CheckState != CheckState.Checked);
            mustHaveChars = string.Join("", letters
                //CheckState.Indeterminate means the word must have this character
                .Where(cb => cb.CheckState == CheckState.Indeterminate)
                .Select(cb => cb.Text.Trim())) +
                string.Join("", nekudos
                .Where(cb => cb != this.cbMelupumBegining && cb.CheckState == CheckState.Indeterminate)
                .Select(cb => cb.Tag));
        }

        private void ShowErrMessage(string message)
        {
            string template = Regex.Replace(Properties.Resources.HTML_TEMPLATE,
                @"__STARTUP_SCRIPT__.+__END_STARTUP_SCRIPT__",
                "*/hideNavBar();/*", RegexOptions.Singleline);
            string html = string.Format("<tr><td style='height:{0}px;' class='errorMessage'>{1}</td></tr>",
                this.pnlResults.Height, message);
            ClientController.CurrentHtml = template.Replace("__TABLE_BODY__", html);
        }

        private void ShowStartupMessage()
        {
            string imgPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                @"Resources\KriahTechLogo.png");
            string template = Regex.Replace(Properties.Resources.HTML_TEMPLATE,
                @"__STARTUP_SCRIPT__.+__END_STARTUP_SCRIPT__",
                "*/showStartup();/*", RegexOptions.Singleline);
            ClientController.CurrentHtml = template.Replace("__TABLE_BODY__", "").Replace("__LOGO_PATH__", imgPath);
        }

        private void InitText()
        {
            this.Text = Path.GetFileNameWithoutExtension(Properties.Settings.Default.CurrentFileName) +
                " - Kriah Tech - " +
                Assembly.GetEntryAssembly().GetName().Version.ToString();
        }

        private void ToggleSearchPanels()
        {
            this.SuspendLayout();
            if (_isSearchPanelOpen)
            {
                while (this.tabControlSearch.Left < this.Width)
                {
                    this.tabControlSearch.Left += 2;
                    this.btnToggleSearch.Left += 2;
                }
                this.tabControlSearch.Visible = false;
                this.tabControlSearch.Left = this.Width - this.tabControlSearch.Width;
                this.tabControlSearch.ResumeLayout();

                this.btnToggleSearch.Left = this.Width - this.btnToggleSearch.Width - 20;
                this.pnlResults.Width += this.tabControlSearch.Width;

                this.btnToggleSearch.BackgroundImage = Properties.Resources.SearchShow;
                _isSearchPanelOpen = false;

            }
            else
            {
                this.tabControlSearch.Left = this.Width;
                this.tabControlSearch.Visible = true;
                while (this.tabControlSearch.Left > (this.Width - this.tabControlSearch.Width))
                {
                    this.tabControlSearch.Left -= 10;
                    this.btnToggleSearch.Left -= 10;
                }
                this.tabControlSearch.Left = this.Width - this.tabControlSearch.Width - 17;
                this.btnToggleSearch.Left = (this.tabControlSearch.Left - this.btnToggleSearch.Width) + 24;
                this.pnlResults.Width -= this.tabControlSearch.Width;
                this.btnToggleSearch.BackgroundImage = Properties.Resources.SearchHide;
                _isSearchPanelOpen = true;
            }
            this.ResumeLayout();
        }


        private void AfterSearch()
        {
            if (Program.CurrentResults.Count() == 0)
            {
                ShowErrMessage("There are no words that match the given criteria.");
            }
            else
            {
                ClientController.RefreshHtml();
                if (Properties.Settings.Default.HideCriteriaAutomatically)
                {
                    this.ToggleSearchPanels();
                }
            }
            this.ToggleSearchButton(true);
        }

        private void InitNekudaTags()
        {
            foreach (var cb in this.pnlNekudos.Controls.OfType<CheckBox>())
            {
                if (cb == this.cbMelupumMiddle)
                {
                    cb.Tag = "וּ";
                }
                else if (cb != this.cbMelupumBegining)
                {
                    cb.Tag = cb.Text.Trim();
                }
            }
        }

        /// <summary>
        /// Build the regex.
        /// </summary>
        /// <param name="letters"></param>
        /// <param name="nekudos"></param>
        /// <param name="ending"></param>
        /// <returns></returns>
        private string BuildRegEx(IEnumerable<CheckBox> letters, IEnumerable<CheckBox> nekudos, IEnumerable<CheckBox> ending)
        {
            StringBuilder regexp = new StringBuilder("^");
            string letterList = string.Join("", letters.Select(l => l.Text.Trim()));
            string nekudosList = string.Join("", nekudos.Select(n => (string)n.Tag));

            if (string.IsNullOrWhiteSpace(letterList))
            {
                letterList = new string(WordProcess.Letters);
            }
            if (string.IsNullOrWhiteSpace(nekudosList) && !(nekudos.Count() == 1 && nekudos.First() == this.cbMelupumBegining))
            {
                nekudosList = new string(WordProcess.Nekudos.Where(n => n != 'וּ').ToArray());
            }
            if (nekudos.Contains(this.cbMelupumBegining))
            {
                regexp.Append("וּ");
                if (this.cbMelupumBegining.CheckState == CheckState.Checked)
                {
                    regexp.Append("?");
                }
            }
            else if (nekudos.Contains(this.cbMelupumMiddle))
            {
                regexp.Append("[^וּ]");
            }
            regexp.AppendFormat("[{0}{1}]+", letterList, nekudosList);

            if (ending.Count() > 0)
            {
                //For endings: Checked: is allowed, Indeterminate: not allowed, UnChecked: required
                //Each word has one ending. If we have a required format, it must be the only ending rule.
                if (ending.Any(cb => cb.CheckState == CheckState.Unchecked))
                {
                    regexp.AppendFormat("{0}", ending.Where(cb =>
                        cb.CheckState == CheckState.Unchecked).First().Text.Trim());
                }
                else
                {
                    //If there is more than one ending in the list, all are not allowed. 
                    //Unlike letters and nekudos, allowed endings are ignored.
                    //A negative look-behind on the end-of-word anchor is used to filter out these endings.
                    //Note: as look-arounds are non-capturing, we won't reach the actual end of the word.
                    regexp.AppendFormat("(?<!({0})$)",
                        string.Join("|", ending.Select(e => e.Text.Trim())));
                }
            }
            regexp.Append("$");

            return regexp.ToString();
        }
        #endregion
    }
}
