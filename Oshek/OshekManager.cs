﻿using System;
using System.IO;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;

namespace Oshek
{
    public static class OshekManager
    {
        private static readonly byte[] _saltBytes = new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 };
        private static char[] _pw = new char[11];

        static OshekManager()
        {

            for (int i = 0, c = 0x5b0; i < _pw.Length; i++)
            {
                _pw[i] = (char)(c + i);
            }
        }

        public static string GetInitiationKey(string programGeneratedRandom)
        {
            return GenerateInitiationKey(programGeneratedRandom);
        }

        public static string GetSerialCode(string initiationKey, string random)
        {
            return Regex.Replace(new string(
                EncryptString(GetHashed(initiationKey.ToUpper()), new string(random.ToLower().Reverse().ToArray()))
                .Reverse().ToArray()), 
                @"[\W]", "").Substring(4, 12).ToUpper();
        }

        public static bool IsValidSerialCode(string serialCode, string random)
        {
            string initKey = GenerateInitiationKey(random);
            return GetSerialCode(initKey, random) == serialCode;
        }

        public static string GetMuxedForStorage(string text)
        {
            string enc = Convert.ToBase64String(Encoding.UTF8.GetBytes(text));
            return EncryptString(enc, new string(_pw));
        }

        public static string UnMuxFromStorage(string text)
        {
            string enc = DecryptString(text, new string(_pw));
            return Encoding.UTF8.GetString(Convert.FromBase64String(enc));
        }

        private static string GenerateInitiationKey(string random)
        {
            string initKey = null;
            try
            {
                using (var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT ProcessorId FROM Win32_Processor"))
                {
                    string procId;
                    foreach (ManagementObject queryObj in searcher.Get())
                    {
                        procId = queryObj["ProcessorId"].ToString();
                        if (!string.IsNullOrWhiteSpace(procId))
                        {
                            string processing = "";
                            for (int i = procId.Length - 1; i >= 0; i--)
                            {
                                char c = procId[i];
                                if (i % 2 == 0)
                                {
                                    processing += random[Math.Min(random.Length - 1, i)];
                                }
                                processing += char.ConvertFromUtf32((int)c + (i * 2));
                            }
                            initKey = Regex.Replace(GetHashed(processing), @"[\W]", "").Substring(4, 9).ToUpper();
                            break;
                        }
                    }
                }
            }
            catch (ManagementException e)
            {
                Console.WriteLine("An error occurred while querying for WMI data: " + e.Message);
            }
            return initKey;
        }

        private static string GetHashed(string text)
        {
            var algorithm = SHA384Managed.Create();
            byte[] data = algorithm.ComputeHash(Encoding.UTF8.GetBytes(text));
            return BitConverter.ToString(data).Replace("-", String.Empty);
        }

        private static string EncryptString(string text, string password)
        {
            byte[] encryptedData;
            byte[] textBytes = Encoding.Unicode.GetBytes(text);
            using (PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, _saltBytes))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (Rijndael alg = Rijndael.Create())
                    {
                        alg.Key = pdb.GetBytes(32);
                        alg.IV = pdb.GetBytes(16);

                        using (CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(textBytes, 0, textBytes.Length);
                            cs.Close();
                        }
                        alg.Clear();
                    }
                    encryptedData = ms.ToArray();
                    ms.Close();
                }
            }
            return Convert.ToBase64String(encryptedData);
        }

        private static string DecryptString(string encryptedText, string password)
        {
            byte[] decryptedData;
            byte[] encryptedBytes = Convert.FromBase64String(encryptedText);

            using (PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, _saltBytes))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (Rijndael alg = Rijndael.Create())
                    {
                        alg.Key = pdb.GetBytes(32);
                        alg.IV = pdb.GetBytes(16);

                        using (CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(encryptedBytes, 0, encryptedBytes.Length);
                            cs.Close();
                        }
                        alg.Clear();
                    }
                    decryptedData = ms.ToArray();
                    ms.Close();
                }
            }
            return Encoding.Unicode.GetString(decryptedData);
        }
    }
}
