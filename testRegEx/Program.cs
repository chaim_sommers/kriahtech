﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace testRegEx
{
    class Program
    {        
        static void Main(string[] args)
        {
            string[] goodList = { "שֶׁעֲווֹנוֹתֵיהֶם", "אָנוּ", "יְראוּ", "אֹנוֹ", "אִמוֹ", "אַפּוֹ", "יִשְׂרָאֵל", "רֵעַ",  "כְּשֶׁהֱחֶזִירַתְהוּ", "וְהַבַּיְתּוֹסִיִין", "אַתְּ", "אָת", "לֵהּ", "לוּ", "לוֹ", "לַך", "לַךְ", "אִשְׁתְּמוֹדְעֵהּ", "תְּחוֹתוֹהִי", "פּוֹתֵחַ" };
            string[] badList = { "ְאַתמִקְלִפּוֹתֵינוּ", "חלכְּשֶׁהֱחֶזִירַתְהוּ", "הַבַּיְתּוֹסִיִינ", "לך", "לֵהֵּאָת", "אפדנַייכוּ", "פּוֹתֵוַ", "פוֹחֲךוּ" };
            Regex re = WordProcess.ValidWordRegEx;
            Console.OutputEncoding = Encoding.Unicode;
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter("text.txt"))
            {
                Console.SetOut(sw);
                Console.WriteLine("Regex used: {0}\nActual Regex: {1}\n{2}", WordProcess.ValidWordRegExString, re.ToString(), new string('-', 55));
                bool hadIssue = false;
                foreach (var w in goodList)
                {
                    if (!re.IsMatch(w))
                    {
                        Console.WriteLine("Good word failed: {0}", w);
                        hadIssue = true;
                    }
                }
                foreach (var w in badList)
                {
                    if (re.IsMatch(w))
                    {
                        Console.WriteLine("Bad word passed: {0}", w);
                        hadIssue = true;
                    }
                }
                if(!hadIssue)
                {
                    Console.WriteLine("<- The regex PASSED all tests. ->");
                }
            }
            Process.Start("text.txt");            
        }

        
    }
}
